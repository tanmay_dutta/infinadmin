import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { platformManqgerProfileDetails } from "store/actions/platformAdminAction";
import { useForm } from "@mantine/form";
import { Button, NativeSelect, TextInput } from "@mantine/core";
import { Dropzone } from "@mantine/dropzone";
import { toast } from "react-toastify";
import { updatePlatformManager } from "store/actions/platformAdminAction";

export default function PlatformManagerEdit() {
  const router = useRouter();
  const platformManagerId = router.query.platformManagerId;
  console.log("professional admin id: ", platformManagerId);
  const dispatch = useDispatch();
  useEffect(() => {
    if (platformManagerId) {
      dispatch(platformManqgerProfileDetails(platformManagerId));
    }
  }, [platformManagerId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.platformManagerProfileDetails
  );
  console.log("result of professional admin user: ", result);

  useEffect(() => {
    form.setValues((prev) => ({ ...prev, ...result }));
  }, [result]);

  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      //email: "",
      mobile: "",
      contactAddress: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  const openRef = useRef(null);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="card admin-card-wedget">
            <form
              onSubmit={form.onSubmit((values) => {
                //toast.success("Plateform Manager Details is Updated Successfully")
                //router.push("/platform-manager")
                dispatch(updatePlatformManager(values));
                console.log("updated values", values);
              })}
            >
              <div className="card-body p-0">
                {result ? (
                  <div className="card-body p-0">
                    <div className="row side-gap-0">
                      {/* Name*/}
                      <div className="item col-lg-6">
                        <TextInput
                          placeholder="firstName"
                          label="First Name"
                          {...form.getInputProps("firstName")}
                        />
                      </div>
                      <div className="item col-lg-6">
                        <TextInput
                          placeholder="lastName"
                          label="Last Name"
                          {...form.getInputProps("lastName")}
                        />
                      </div>
                      {/* Contact number*/}
                      <div className="item col-lg-6">
                        <TextInput
                          placeholder="Contact number"
                          label="Contact number"
                          {...form.getInputProps("mobile")}
                        />
                      </div>
                      {/* Email ID*/}
                      <div className="item col-lg-6">
                        <TextInput placeholder="Email ID" label="Email ID" {...form.getInputProps("email")} />

                      </div>
                      {/* Contact Address*/}
                      <div className="item col-lg-12">
                        <TextInput
                          placeholder="Contact Address"
                          label="Contact Address"
                          {...form.getInputProps("contactAddress")}
                        />
                      </div>

                      {/* Update Button*/}

                      <div className="col-lg-12 actions place-center mt-5">
                        <Button type="submit">Update</Button>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
