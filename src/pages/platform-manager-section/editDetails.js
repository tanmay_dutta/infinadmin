import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import { useForm } from "@mantine/form";
import SideMenu from "@/components/sideMenu";
import { Button, TextInput } from "@mantine/core";
import { useDispatch, useSelector } from "react-redux";
import { platformManagerUpdateProfileDetails } from "store/actions/platformManagerAction";
import { platformManagerProfileDetails } from "store/actions/platformManagerAction";
import { useRef, useEffect } from "react";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import { Dropzone } from "@mantine/dropzone";

export default function SimpleDetailsEdit() {
  const openRef = useRef(null);
  const dispatch = useDispatch();
  const router = useRouter();
  useEffect(() => {
    dispatch(platformManagerProfileDetails());
  }, []);
  const result = useSelector(
    (state) => state.platformManagerReducer.platformManagerData
  );
  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      mobile: "",
      contactAddress: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  useEffect(() => {
    form.setValues((prev) => ({
      ...prev,
      ...result,
    }));
  }, [result]);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="card admin-card-wedget">
              <form
                onSubmit={form.onSubmit((values) => {
                  dispatch(platformManagerUpdateProfileDetails(values));
                })}
              >
                <div className="card-body p-0">
                  <div className="row side-gap-0">
                    <div className="col-lg-6">
                      <TextInput
                        placeholder="First name"
                        {...form.getInputProps("firstName")}
                      />
                    </div>
                    <div className="col-lg-6">
                      <TextInput
                        placeholder="Last name"
                        {...form.getInputProps("lastName")}
                      />
                    </div>

                    <div className="col-lg-12">
                      <TextInput
                        placeholder="Contact number"
                        {...form.getInputProps("mobile")}
                      />
                    </div>

                    <div className="col-lg-12">
                      <TextInput
                        placeholder="Contact Address"
                        {...form.getInputProps("contactAddress")}
                      />
                    </div>

                    {/* dropzone */}
                    <div className="col-lg-12">
                      <Dropzone
                        openRef={openRef}
                        onDrop={(files) => {
                          console.log("---===", files);
                          console.log("===---", files[0]);
                          // console.log("accepted files", files);
                          // selectedFile.push(files);
                        }}
                        // onDrop={handleFileDrop}
                        activateOnClick={false}
                        className="file-select-input"
                        styles={{ inner: { pointerEvents: "all" } }}
                      >
                        {/* button*/}
                        <button
                          onClick={() => openRef.current()}
                          className="file-load-btn"
                        >
                          <span className="icon">
                            <svg
                              width="34"
                              height="34"
                              viewBox="0 0 34 34"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fillRule="evenodd"
                                clipRule="evenodd"
                                d="M16.9669 -7.60645e-08C17.9279 -3.40552e-08 18.707 0.779091 18.707 1.74015L18.707 32.1927C18.707 33.1538 17.9279 33.9329 16.9669 33.9329C16.0058 33.9329 15.2267 33.1538 15.2267 32.1927L15.2267 1.74015C15.2267 0.779091 16.0058 -1.18074e-07 16.9669 -7.60645e-08Z"
                                fill="#47B6F4"
                              />
                              <path
                                fillRule="evenodd"
                                clipRule="evenodd"
                                d="M0 17.8368C0 16.8758 0.779091 16.0967 1.74015 16.0967L32.1927 16.0967C33.1538 16.0967 33.9329 16.8758 33.9329 17.8368C33.9329 18.7979 33.1538 19.577 32.1927 19.577L1.74015 19.577C0.779091 19.577 0 18.7979 0 17.8368Z"
                                fill="#47B6F4"
                              />
                            </svg>
                          </span>
                          Upload Documents/ID
                        </button>
                      </Dropzone>
                    </div>

                    <div className="col-lg-12 actions place-center mt-5">
                      <Button type="submit">Update</Button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
