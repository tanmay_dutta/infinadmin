import { Avatar, Menu } from "@mantine/core";
import VerticalDot from "@/components/icon/vertical-dot";
import Edit from "../icon/edit";
import Delete from "../icon/delete";
import View from "../icon/view";
import CsvDownload from "../CsvDownload";
import { useDispatch } from "react-redux";
import { verifyProfessionalAdmin } from "store/actions/platformAdminAction";
import { deletePlatformClientAdmin } from "store/actions/platformAdminAction";


//main component for client admin list
export default function ClientAdminList(props) {
  // const comingSoonPopup = ()=>{
  //   alert("Coming soon");
  // }
  const dispatch = useDispatch();
  const headers = [
    { label: "firstName", key: "firstName" },
    { label: "lastName", key: "lastName" },
    { label: "Contact number", key: "mobile" },
    { label: "Email ID", key: "email" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "Valid Proof", key: "validIdProof" },
  ];

  let data = [
    {
      firstName: `${props.firstName} `,
      lastName: `${props.lastName}`,
      mobile: `${props.mobile}`,
      email: `${props.email}`,
      contactAddress: `${props.communicationAddress}`,
      uploadedDocument: `${props.uploadedDocument}`,
      validIdProof: `${props.validIdProof}`,
    },
  ];

  return (
    <div className="card admin-card-wedget">
      <div className="card-head">
         {/* Avatar*/}
        <div className="card-image">
          <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
        </div>
         {/* GST Number*/}
        <div className="info">
          <h3 className="card-title">{props.firstName}{" "}{props.lastName}</h3>
        </div>
          {/* Download*/}
          <div
          className="btns ms-auto mb-auto d-flex flex-wrap">


              {props ? (
                <CsvDownload
                  data={data}
                  headers={headers}
                  filename={"Client admin Detail"}
                />
              ) : (
                ""
              )}
        </div>
        { props.isVerified == 0?
        
        
        <div onClick={(e)=>{e.preventDefault();dispatch(verifyProfessionalAdmin(props.id))}} className="btn btn-sm btn-light-blue mb-auto d-flex flex-wrap">Verify</div>
      
       :'Verified'}
        {/* Menu Dropdown with VerticalDot*/}
        <Menu shadow="md" width={200} classNames="threedot-drops-menu">
          <Menu.Target>
            <div className="three-dot-btn">
              <VerticalDot />
            </div>
          </Menu.Target>
          <Menu.Dropdown>
          <Menu.Item component="a" href= {`/client-admin/${props.id}`} icon={<View size={14} />} >View</Menu.Item>

          <Menu.Item component="a" href=  {`/client-admin-edit/${props.id}`} icon={<Edit />} >Edit</Menu.Item>
          <Menu.Item icon={<Delete />} onClick={(e)=>{e.preventDefault();dispatch(deletePlatformClientAdmin(props))}}>Delete</Menu.Item>
          </Menu.Dropdown>
        </Menu>
      </div>
      <div className="card-body">
        <div className="key-val-lists row">
          {/* Contact number*/}
          <div className="item col-lg-6">
            <span className="key">Contact number: </span>
            <span className="val">{props.mobile}</span>
          </div>
          {/* BusinessType*/}
          <div className="item col-lg-6">
            <span className="key">BusinessType: </span>
            <span className="val">{props.businessType}</span>
          </div>
          {/* Email ID*/}
          <div className="item col-lg-6">
            <span className="key">Email ID: </span>
            <span className="val">{props.email}</span>
          </div>
          {/* Company Name*/}
          <div className="item col-lg-6">
            <span className="key">Company Name: </span>
            <span className="val">{props.companyName}</span>
          </div>
          {/* PAN Number*/}
          <div className="item col-lg-6">
            <span className="key">PAN Number: </span>
            <span className="val">{props.panNumber}</span>
          </div>
          <div className="item col-lg-6">
            <span className="key">GST Number: </span>
            <span className="val">{props.gstNumber}</span>
          </div>
          {/* Subscribed plan*/}
          <div className="item col-lg-6">
            <span className="key">Subscribed plan: </span>
            <span className="val">
              <span className="pill pill-light-blue">Professional</span>
            </span>
          </div>
          {/* Status*/}
          <div className="item col-lg-12">
            <span className="key">Status: </span>
            <span className="val">{props.createdBy}</span>
          </div>
          {/* Contact Address*/}
          <div className="item col-lg-12">
            <span className="key">Contact Address: </span>
            <span className="val"> {props.communicationAddress}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
