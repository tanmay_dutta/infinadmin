import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clientlAdminProfileDetails } from "store/actions/platformAdminAction";
import { useRouter } from "next/router";

export default function ClientDetails() {
  const router = useRouter()
  const clientId = router.query.clientId;
  console.log("client admin id: ", clientId);
  const dispatch = useDispatch();
  useEffect(() => {
    if (clientId) {
      dispatch(clientlAdminProfileDetails(clientId)); // action calling
    }
  }, [clientId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.clientAdminProfileDetails
  );
  console.log("result of client admin user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        {result ? <> <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                 {/* Name*/}
              <div className="item col-lg-6">
                  <span className="key">Name: </span>
                  <span className="val">{result.name}</span>
                </div>
                 {/* Contact number*/}
                <div className="item col-lg-6">
                  <span className="key">Contact number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
               {/* BusinessType*/}
                <div className="item col-lg-6">
                  <span className="key">BusinessType: </span>
                  <span className="val">{result.businessType}</span>
                </div>
               {/* Email ID*/}
                <div className="item col-lg-6">
                  <span className="key">Email ID: </span>
                  <span className="val">{result.email}</span>
                </div>
                  {/*Company Name*/}
                <div className="item col-lg-6">
                  <span className="key">Company Name: </span>
                  <span className="val">{result.companyName}</span>
                </div>
                 {/* PAN Number*/}
                <div className="item col-lg-6">
                  <span className="key">PAN Number: </span>
                  <span className="val">{result.panNumber}</span>
                </div>
                 {/* Status*/}
                <div className="item col-lg-12">
                  <span className="key">Status: </span>
                  <span className="val">{result.createdBy}</span>
                </div>
                  {/* Contact Address*/}
                <div className="item col-lg-12">
                  <span className="key">Contact Address: </span>
                  <span className="val"> {result.communicationAddress}</span>
                </div>
              </div>
            </div>
          </div>
        </div> </> : null}
      </div>
   {/* Footer*/}
      <Footer />
    </>
  );
}
