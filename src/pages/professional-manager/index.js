import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { professionalAdminProfileDetails } from "store/actions/platformAdminAction";

export default function ProfessionalManagerDetails() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(professionalAdminProfileDetails());
  }, []);

  const result = useSelector(
    (state) => state.platformAdminReducer.professionalAdminDetails);
  console.log("result of professional admin user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">{result.name}</span>
                </div>
                <div className="item">
                  <span className="key">Membership Number: </span>
                  <span className="val">{result.membershipNumber}</span>
                </div>
                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                <div className="item">
                  <span className="key">Contact address: </span>
                  <span className="val">{result.contactAddress}</span>
                </div>
                <div className="item">
                  <span className="key">Created By: </span>
                  <span className="val">{result.createdBy}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
