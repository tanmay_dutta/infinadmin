import {
  Button,
  Checkbox,
  PasswordInput,
  TextInput,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import { useDispatch } from "react-redux";
import { handleLogin } from "store/actions/platformAdminAction";
import Link from "next/link";
import Image from "next/image";
export default function Login() {
  const dispatch = useDispatch();

 

  const form = useForm({
    initialValues: {
      email: "",
      password: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
    },
  });
  // function afterLogin if token then go to dashboard
  return (
    <div className="user-auth-pages">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-6 user-auth-left-card">
            <div className="card">
              <div className="brand">
                <Link href="">
                  <Image src="/logo.png" alt=""  width={100} height={45}/>
                </Link>
              </div>
              <h1 className="card-title">All in one workspace</h1>
              <p className="card-text">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration.
              </p>
              <div className="card-image">
                <Image src="/login-left-bg.png" alt="" className="card-img"  width={1000} height={1000}/>
              </div>
            </div>
          </div>
          <div className="col-lg-6 user-rorm-card bg-white">
            <div className="form-wrap">
              <h2 className="card-title">Login</h2>
              <form
                onSubmit={form.onSubmit((values) =>
                  dispatch(handleLogin(values))
                )}
              >
                <div className="row">
                  <div className="col-lg-12">
                    <TextInput
                      withAsterisk
                      placeholder="Email ID"
                      {...form.getInputProps("email")}
                    />
                  </div>
                  <div className="col-lg-12">
                    <PasswordInput
                      placeholder="Password"
                      withAsterisk
                      {...form.getInputProps("password")}
                    />
                  </div>
                </div>
                <div className="row pb-5">
                  <div className="col forget_check_link">
                    <Checkbox label="Remember me" />
                    <Link href="/forgot-password">Forget password?</Link>
                  </div>
                </div>
                <div className="row actions mt-5">
                  <div className="col ">
                    <Button type="submit" className="mb-3 submit-btn">
                      Submit
                    </Button>
                    
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
