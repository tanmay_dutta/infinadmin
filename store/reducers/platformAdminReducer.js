import {
  AUTH_LOGIN_START,
  AUTH_LOGIN,
  AUTH_ERROR,
  PLATFORM_ADMIN_DETAILS,
  PLATFORM_ADMIN_DETAILS_ERROR,
  GET_PROFESSIONAL_ADMIN_LIST,
  GET_PROFESSIONAL_ADMIN_LIST_ERROR,
  GET_CLIENT_ADMIN_LIST,
  GET_CLIENT_ADMIN_LIST_ERROR,
  UPDATE_PLATFORM_PROFILE_DETAILS,
  UPDATE_PLATFORM_PROFILE_DETAILS_ERROR,
  PLATFORM_USER_PROFILE_DETAILS,
  PLATFORM_USER_PROFILE_DETAILS_ERROR,
  PROFESSIONAL_ADMIN_PROFILE_DETAILS,
  PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
  CLIENT_ADMIN_PROFILE_DETAILS,
  CLIENT_ADMIN_PROFILE_DETAILS_ERROR,
  PLATFORM_MANAGER_PROFILE_DETAILS,
  PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
  CREATE_PLATFORM_USER,
  CREATE_PLATFORM_USER_ERROR,
  PLATFORM_MANAGER_LIST,
  PLATFORM_MANAGER_LIST_ERROR,
  PLATFORM_USER_LIST,
  PLATFORM_USER_LIST_ERROR,
  FORGET_PASSWORD,
  FORGET_PASSWORD_ERROR,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
  UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS,
  UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
  VERIFY_PROFESSIONAL_ADMIN,
  VERIFY_PROFESSIONAL_ADMIN_ERROR,
  UPDATE_PLATFORM_USER,
  UPDATE_PLATFORM_USER_ERROR,
  UPDATE_CLIENT_ADMIN,
  UPDATE_CLIENT_ADMIN_ERROR,
  DELTE_PLATFORM_MANAGER,
  DELTE_PLATFORM_MANAGER_ERROR,
  DELETE_PLATFORM_USER,
  DELETE_PLATFORM_USER_ERROR,
  DELETE_PROFESSIONAL_ADMIN,
  DELETE_PROFESSIONAL_ADMIN_ERROR,
  DELTE_CLIENT_ADMIN,
  DELTE_CLIENT_ADMIN_ERROR,

} from "../types";


const ISSERVER = typeof window === "undefined";

if (!ISSERVER) {
  var localValue = !!localStorage.getItem("token_key");
}

// initial states initialization for paltfromAdminReducer
const initialState = {
  isLoggedIn: localValue,
  success: false,
  loading: false,
  authError: false,
  authErrorMsg: "",
  platformAdminData: {},
  getProfessionalAdminList: {},
  getClientAdminList: {},
  updatePlatformdetails: {},
  platformUserProfileDetails: {},
  professionalAdminDetails: {},
  clientAdminProfileDetails: {},
  platformManagerProfileDetails: {},
  createPlatformUserStatus: {},
  platformManagerList: {},
  platformUserList:{},
  updateProfessionalAdminDetails: {},
  updatePlatformManagerDetails: {},
  deletePlatformManager: {},
  deletePlatformUser:{},
  deleteProfessionalUser:{},
  deleteClientAdmin: {},


};

export default function (state = initialState, action) {
  switch (action.type) {
//state change of reducer based of types of action

    case AUTH_LOGIN_START:
      return {
        ...state,
        loading: true,
      };

    case AUTH_LOGIN:
      return {
        ...state,
        isLoggedIn: !!localStorage.getItem("token_key"),
        token: action.payload,
        success: true,
        loading: false,
      };

    case AUTH_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        token: "",
        authError: true,
        authErrorMsg: action.payload,
      };
    case PLATFORM_ADMIN_DETAILS:
      return {
        ...state,
        platformAdminData: action.payload,
        loading: false,
      };
    case PLATFORM_ADMIN_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case GET_PROFESSIONAL_ADMIN_LIST:
      return {
        ...state,
        getProfessionalAdminList: action.payload,
        loading: false,
      };
    case GET_PROFESSIONAL_ADMIN_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };
    case GET_CLIENT_ADMIN_LIST:
      return {
        ...state,
        getClientAdminList: action.payload,
        loading: false,
      };
    case GET_CLIENT_ADMIN_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_PLATFORM_PROFILE_DETAILS:
      return {
        ...state,
        updatePlatformdetails: action.payload,
        loading: false,
      };
    case UPDATE_PLATFORM_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };
    case PLATFORM_USER_PROFILE_DETAILS:
      return {
        ...state,
        platformUserProfileDetails: action.payload,
        loading: false,
      };
    case PLATFORM_USER_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case PROFESSIONAL_ADMIN_PROFILE_DETAILS:
      return {
        ...state,
        professionalAdminDetails: action.payload,
        loading: false,
      };
    case PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case CLIENT_ADMIN_PROFILE_DETAILS:
      return {
        ...state,
        clientAdminProfileDetails: action.payload,
        loading: false,
      };
    case CLIENT_ADMIN_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case PLATFORM_MANAGER_PROFILE_DETAILS:
      return {
        ...state,
        platformManagerProfileDetails: action.payload,
        loading: false,
      };

    case PLATFORM_MANAGER_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case CREATE_PLATFORM_USER:
      return {
        ...state,
        createPlatformUserStatus: action.payload,
        loading: false,
      };
    case CREATE_PLATFORM_USER_ERROR:
      return {
        ...state,
        loading: true,
      };

      case PLATFORM_MANAGER_LIST:
      return {
        ...state,
        platformManagerList: action.payload,
        loading: false,
      };
      case PLATFORM_MANAGER_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };
      case PLATFORM_USER_LIST:
      return {
        ...state,
        platformUserList: action.payload,
        loading: false,
      };
      case PLATFORM_USER_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };
      case UPDATE_PLATFORM_USER:
      return {
        ...state,
        loading: false,
      };
    case UPDATE_PLATFORM_USER_ERROR:
      return {
        ...state,
        loading: true,
      };
      case UPDATE_CLIENT_ADMIN:
      return {
        ...state,
        loading: false,
      };
    case UPDATE_CLIENT_ADMIN_ERROR:
      return {
        ...state,
        loading: true,
      };
      case FORGET_PASSWORD:
        return {
          ...state,
          loading: true,
        };
      case FORGET_PASSWORD_ERROR:
        return {
          ...state,
          authError: true,
          authErrorMsg: action.payload,
        }

        case UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS:
          return {
            ...state,
            updateProfessionalAdminDetails:action.payload,
            loading: false,
          };
        case UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR:
          return {
            ...state,
            loading: true,
          };

          case UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS:
            return {
              ...state,
              updatePlatformManagerDetails:action.payload,
              loading: false,
            };
          case UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS_ERROR:
            return {
              ...state,
              loading: true,
            };
        case VERIFY_PROFESSIONAL_ADMIN,VERIFY_PROFESSIONAL_ADMIN_ERROR:
          return {
            ...state,
            loading: false,
          };
          case DELTE_PLATFORM_MANAGER:
            return {
              ...state,
              deletePlatformManager: action.payload,
              loading: false,
            };
            case DELTE_PLATFORM_MANAGER_ERROR:
            return {
              ...state,
              loading: true,
            };
            case DELETE_PLATFORM_USER:
            return {
              ...state,
              deletePlatformUser: action.payload,
              loading: false,
            };
            case DELETE_PLATFORM_USER_ERROR:
            return {
              ...state,
              loading: true,
            };
            case DELETE_PROFESSIONAL_ADMIN:
            return {
              ...state,
              deleteProfessionalUser: action.payload,
              loading: false,
            };
            case DELETE_PROFESSIONAL_ADMIN_ERROR:
            return {
              ...state,
              loading: true,
            };
            case DELTE_CLIENT_ADMIN:
            return {
              ...state,
              deleteClientAdmin: action.payload,
              loading: false,
            };
            case DELTE_CLIENT_ADMIN_ERROR:
            return {
              ...state,
              loading: true,
            };

    default:
      return state;
  }
}
