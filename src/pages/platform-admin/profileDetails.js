import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { platFormAdminProfileDetails } from "store/actions/platformAdminAction";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function SimpleDetails() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(platFormAdminProfileDetails());
  }, [dispatch]);

  const result = useSelector((state) => state.platformAdminReducer);
  console.log("result is", result);
  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">
                    {result.platformAdminData.firstName}{" "}
                    {result.platformAdminData.lastName}
                  </span>
                </div>

                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.platformAdminData.mobile}</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.platformAdminData.email}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
