import { combineReducers } from "redux";
import platformAdminReducer from "./platformAdminReducer";
import platformManagerReducer from "./platformManagerReducer";


//Root Reducer
export default combineReducers({
    platformAdminReducer: platformAdminReducer,
    platformManagerReducer: platformManagerReducer

});