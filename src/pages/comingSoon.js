import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import ProfessionalAdminCard from "@/components/list/ProfessionalAdminCard";
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from "react";



export default function ProfessionalManager() {
  useEffect(() => {
    
  }, [])
  
    return <>
    <div className='admin_wrap container-fluid'>
       {/* SideMenu */}
          <SideMenu />
          <div className="admin-content-area">
            {/* AdminHeader */}
            <AdminHeader />
            <div className="dashboard-widget-wrap">
            <h2>Coming Soon</h2>

            </div>
          </div>
        </div>
          {/*Footer */}
        <Footer />
    </>
  }