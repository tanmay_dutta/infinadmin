import "bootstrap/dist/css/bootstrap.css";
import "@/styles/globals.scss";
import Layout from "../components/layout";
import { Provider } from "react-redux";
import { store } from "store/store";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress
import Router from "next/router";

export default function App({ Component, pageProps }) {

  NProgress.configure({ easing: 'ease', speed: 200, showSpinner: false })
  Router.events.on('routeChangeStart', () => NProgress.start());
  Router.events.on('routeChangeComplete', () => NProgress.done());
  Router.events.on('routeChangeError', () => NProgress.done());
  return (
    <>
      <Layout>
        <Provider store={store}>

          <Component {...pageProps} />

          <ToastContainer
            position="top-right"
            autoClose={2000}
            hideProgressBar={false}
            newestOnTop={false}
            draggable={false}
            pauseOnVisibilityChange
            closeOnClick
            pauseOnHover
          />
        </Provider>
      </Layout>
    </>
  );
}
