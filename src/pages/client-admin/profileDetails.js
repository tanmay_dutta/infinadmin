import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clientlAdminProfileDetails } from "store/actions/platformAdminAction";

export default function ProfileDetails() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(clientlAdminProfileDetails());
  }, []);

  const result = useSelector(
    (state) => state.platformAdminReducer.clientAdminProfileDetails);
  console.log("result of platform user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">BusinessType: </span>
                  <span className="val">{result.businessType}</span>
                </div>
                <div className="item">
                  <span className="key">CompanyName: </span>
                  <span className="val">{result.companyName}</span>
                </div>
                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                <div className="item">
                  <span className="key">GST NO. : </span>
                  <span className="val">{result.gstNumber}</span>
                </div>
                <div className="item">
                  <span className="key">PAN NO. : </span>
                  <span className="val">{result.panNumber}</span>
                </div>
                <div className="item">
                  <span className="key">Contact address: </span>
                  <span className="val">{result.communicationAddress}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
