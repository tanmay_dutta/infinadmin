import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import ProfessionalAdminCard from "@/components/list/ProfessionalAdminCard";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getProfessionalAdminList } from "store/actions/platformAdminAction";
import LoadingScreen from "../loading";
import CsvDownload from "@/components/CsvDownload";


import { Avatar, Menu } from "@mantine/core";
import DownloadIcon from "@/components/icon/download";
import VerticalDot from "@/components/icon/vertical-dot";
import Edit from "@/components/icon/edit";
import Delete from "@/components/icon/delete";
import View from "@/components/icon/view";
import { CSVLink } from "react-csv";

//Main
export default function ProfessionalAdmin(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProfessionalAdminList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformAdminReducer.getProfessionalAdminList
  );
  const professional_Admin_List = result.content;
console.log("sanghaaaaaaaaaa",professional_Admin_List);
  //setting up download total list

  const headers = [
    { label: "ID", key: "id" },
    { label: "First Name", key: "firstName" },
    { label: "Last Name", key: "lastName" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];
  const handleClick = () => {
    alert("Comming Soon");
  }
  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />

          {/* Download */}
          <div
         className="top-filter-btns-group ms-auto d-flex flex-wrap">


              {professional_Admin_List ? (
                <CsvDownload
                  data={professional_Admin_List}
                  headers={headers}
                  filename={"Professional Admin List"}
                />
              ) : (
                ""
              )}
        </div>

          {professional_Admin_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {professional_Admin_List?.map((item, key) => {
                return (
                  //ProfessionalAdminCard
                  <ProfessionalAdminCard
                    id={item.id}
                    contactAddress={item.contactAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    membershipNumber={item.membershipNumber}
                    mobile={item.mobile}
                    isVerified={item.isVerified}
                    professionalAdminDetailId={item.professionalAdminDetailId}
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
              <ProfessionalAdminCard />
            </div>
          ) : (
            //<LoadingScreen />
            <div className="dashboard-widget-wrap">


                
                <div className="card admin-card-wedget">
      <div className="card-head">
        {/* Avatar card-image*/}
        <div className="card-image">
          <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
        </div>
        {/* Serial Number*/}
        <div className="info">
          <h3 className="card-title">firstName lastName</h3>
        </div>
        {/* Download */}
        <div
          className="btns ms-auto mb-auto d-flex flex-wrap">


              
<CSVLink className="btn btn-sm btn-light-blue" data={`text`} headers={headers} filename={`demo.csv`}
        >
         <span className="icon"><DownloadIcon/>Download Details</span>
         
        </CSVLink>
             
        </div>
       
        
        
        <div className="btn btn-sm btn-light-blue mb-auto d-flex flex-wrap">Verify</div>
      
      
        
        {/* Menu Dropdown with VerticalDot*/}
        <Menu shadow="md" width={200} classNames="threedot-drops-menu">
          <Menu.Target>
            <div className="three-dot-btn">
              <VerticalDot />
            </div>
          </Menu.Target>
          <Menu.Dropdown>
            <Menu.Item
              component="a"
              onClick = {() => handleClick()}
              icon={<View />}
            >
              {" "}
              View
            </Menu.Item>
            <Menu.Item
              icon={<Edit />}
              component="a"
              onClick = {() => handleClick()}
            >
              Edit
            </Menu.Item>
            {/* <Menu.Item icon={<Delete />} >
              Delete
            </Menu.Item> */}
          </Menu.Dropdown>
        </Menu>
      </div>
      <div className="card-body">
        <div className="key-val-lists row">
          
          {/* Contact number*/}
          <div className="col-lg-8">

       

            <div className="item">
              <span className="key">Contact number: </span>
              <span className="val">1234567898</span>
            </div>
            {/* Email ID*/}
            <div className="item">
              <span className="key">Email ID: </span>
              <span className="val text-truncate">test@gmail.com</span>
            </div>

            {/* Status*/}
            <div className="item">
              <span className="key">Status: </span>
              <span className="val"> FCA</span>
            </div>
            {/* Contact Address*/}
            <div className="item">
              <span className="key">Contact Address: </span>
              <span className="val"> Test test test</span>
            </div>

            {/* Uploaded Document*/}
            <div className="item">
              <span className="key">Uploaded Document: </span>
              <span className="val"> Test</span>
            </div>
            {/* Valid Proof*/}
            <div className="item">
              <span className="key">Valid Proof: </span>
              <span className="val"> Test</span>
            </div>
          </div>
          {/* Years of experience*/}
          <div className=" col-lg-4">
            <div className="item">
              <span className="key">Years of experience: </span>
              <span className="val">10 years +</span>
            </div>
            {/* Subscribed plan*/}
            <div className="item">
              <span className="key">Subscribed plan: </span>
              <span className="val">
                <span className="pill pill-light-blue small">Professional</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div className="card admin-card-wedget">
      <div className="card-head">
        {/* Avatar card-image*/}
        <div className="card-image">
          <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
        </div>
        {/* Serial Number*/}
        <div className="info">
          <h3 className="card-title">firstName lastName</h3>
        </div>
        {/* Download */}
        <div
          className="btns ms-auto mb-auto d-flex flex-wrap">


              
<CSVLink className="btn btn-sm btn-light-blue" data={`text`} headers={headers} filename={`demo.csv`}
        >
         <span className="icon"><DownloadIcon/>Download Details</span>
         
        </CSVLink>
             
        </div>
       
        
        
        <div className="btn btn-sm btn-light-blue mb-auto d-flex flex-wrap">Verify</div>
      
      
        
        {/* Menu Dropdown with VerticalDot*/}
        <Menu shadow="md" width={200} classNames="threedot-drops-menu">
          <Menu.Target>
            <div className="three-dot-btn">
              <VerticalDot />
            </div>
          </Menu.Target>
          <Menu.Dropdown>
            <Menu.Item
              component="a"
              onClick = {() => handleClick()}
              icon={<View />}
            >
              {" "}
              View
            </Menu.Item>
            <Menu.Item
              icon={<Edit />}
              component="a"
              onClick = {() => handleClick()}
            >
              Edit
            </Menu.Item>
            {/* <Menu.Item icon={<Delete />} >
              Delete
            </Menu.Item> */}
          </Menu.Dropdown>
        </Menu>
      </div>
      <div className="card-body">
        <div className="key-val-lists row">
          
          {/* Contact number*/}
          <div className="col-lg-8">

       

            <div className="item">
              <span className="key">Contact number: </span>
              <span className="val">1234567898</span>
            </div>
            {/* Email ID*/}
            <div className="item">
              <span className="key">Email ID: </span>
              <span className="val text-truncate">test@gmail.com</span>
            </div>

            {/* Status*/}
            <div className="item">
              <span className="key">Status: </span>
              <span className="val"> FCA</span>
            </div>
            {/* Contact Address*/}
            <div className="item">
              <span className="key">Contact Address: </span>
              <span className="val"> Test test test</span>
            </div>

            {/* Uploaded Document*/}
            <div className="item">
              <span className="key">Uploaded Document: </span>
              <span className="val"> Test</span>
            </div>
            {/* Valid Proof*/}
            <div className="item">
              <span className="key">Valid Proof: </span>
              <span className="val"> Test</span>
            </div>
          </div>
          {/* Years of experience*/}
          <div className=" col-lg-4">
            <div className="item">
              <span className="key">Years of experience: </span>
              <span className="val">10 years +</span>
            </div>
            {/* Subscribed plan*/}
            <div className="item">
              <span className="key">Subscribed plan: </span>
              <span className="val">
                <span className="pill pill-light-blue small">Professional</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>




          </div>
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
