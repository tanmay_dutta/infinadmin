import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";

//main
export default function SimpleDetails() {
    return <>
     <div className="admin_wrap container-fluid">
         {/* SideMenu*/}
            <SideMenu />
            <div className="admin-content-area">
                 {/* AdminHeader*/}
                <AdminHeader />
                <div className="dashboard-widget-wrap">
                    <div className="block-content-box profile-info-details-card">
                        <div className="key-val-items">
                              {/* Contact Number*/}
                            <div className="item">
                                <span className="key">Contact Number: </span>
                                <span className="val">1234 5678 910</span>
                            </div>
                              {/* Email address*/}
                            <div className="item">
                                <span className="key">Email address: </span>
                                <span className="val"> demomail@gmail.com</span>
                            </div>
                              {/* Membership number*/}
                            <div className="item">
                                <span className="key">Membership number: </span>
                                <span className="val">ABC123456 ASC</span>
                            </div>
                              {/* Contact address*/}
                            <div className="item">
                                <span className="key">Contact address: </span>
                                <span className="val">Ap285-7193 Ullamcorper Avenue
                                  Amesbury HI 93373</span>
                            </div>
                        
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {/* Footer*/}
        <Footer />
    </>
}