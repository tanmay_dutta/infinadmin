import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { professionalAdminProfileDetailstest } from "store/actions/platformManagerAction";
import { updateProfessionalAdminProfile } from "store/actions/platformManagerAction";
import { useForm } from "@mantine/form";
import { Button, NativeSelect, TextInput } from "@mantine/core";
import { Dropzone } from "@mantine/dropzone";
import { toast } from "react-toastify";
export default function UserDetailsEdit() {

  const router = useRouter()
  const updateAdminDetails  = router.query.updateAdminDetails;
  console.log("professional admin id: ", updateAdminDetails);
  const dispatch = useDispatch();
  useEffect(() => {
    if (updateAdminDetails) {
      dispatch(professionalAdminProfileDetailstest(updateAdminDetails)); // action calling
    }


  }, [updateAdminDetails, dispatch]);

  const result = useSelector(
    (state) => state.platformManagerReducer.professionalAdminProfileDetails);
  console.log("result of user: ", result);

  useEffect(() => {
    form.setValues((prev) => ({ ...prev, ...result }));
  }, [result])

  const form = useForm({
    initialValues: {
      firstName:"",
      lastName:"",
      mobile: "",
      contactAddress: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  const openRef = useRef(null);

  return (
    <>
     

<div className="admin_wrap container-fluid">
       {/* SideMenu*/}
      <SideMenu />
      <div className="admin-content-area">
         {/*AdminHeader*/}
        <AdminHeader />
        <div className="card admin-card-wedget">
      <form
        onSubmit={form.onSubmit((values) =>

          {
         
            // toast.success("Client Admin Details Updated Successfully");
            dispatch(updateProfessionalAdminProfile(values));
          }
        )}
      >
        <div className="card-body p-0">
        { 
        result?
        <div className="row side-gap-0">
        {/* Name*/}
         <div className="col-lg-6">
           <TextInput  placeholder="Name" label="First Name"  {...form.getInputProps("firstName")}/>
         </div>
         <div className="col-lg-6">
           <TextInput  placeholder="Name" label="Last Name"  {...form.getInputProps("lastName")}/>
         </div>
          {/* Email address*/}
         {/* <div className="col-lg-12">
           <TextInput placeholder="Email address" {...form.getInputProps("email")}/>
         </div> */}
           {/* Contact number*/}
         <div className="col-lg-12">
           <TextInput placeholder="Contact number" label="Contact Name" {...form.getInputProps("mobile")}/>
         </div>
         {/* Location*/}
         <div className="col-lg-12">
           <TextInput placeholder="Location" label="Location" {...form.getInputProps("contactAddress")}/>
         </div>
        
           {/* Update Button*/}
        
         <div className="col-lg-12 actions place-center mt-5">
           <Button type="submit">Update</Button>
         </div>
       </div>
    :null  
    }
        </div>
      </form>
    </div>
      </div>
    </div>
      {/* Footer*/}
    <Footer />
    </>
  );
}

