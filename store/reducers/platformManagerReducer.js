import {
  PROFESSIONAL_ADMIN_LIST,
  PROFESSIONAL_ADMIN_LIST_ERROR,
  GET_PLATFORM_MANAGER_PROFILE,
  GET_PLATFORM_MANAGER_PROFILE_ERROR,
  PLATFORM_MANAGER_UPDATE_PROFILE,
  PLATFORM_MANAGER_UPDATE_PROFILE_ERROR,
  PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST,
  PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST_ERROR,
  PLATFORM_MANAGER_USER_PROFILE_DETAILS,
  PLATFORM_MANAGER_USER_PROFILE_DETAILS_ERROR,
  PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE,
  PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE_ERROR,
  GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
  GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE_ERROR,
} from "../types";

const ISSERVER = typeof window === "undefined";

if (!ISSERVER) {
  var localValue = !!localStorage.getItem("token_key");
}

const initialState = {
  isLoggedIn: localValue,
  success: false,
  loading: false,
  getProfessionalAdminList: {},
  platformManagerData: {},
  professionalAdminProfileDetails: {},
  updateProfessionalAdminProfile: {},
  plaformUserList:{},
  platformUserDetails:{},
  updateUserProfileDetails:{},
  platformManagerUpdate:{}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case PROFESSIONAL_ADMIN_LIST:
      return {
        ...state,
        getProfessionalAdminList: action.payload,
        loading: false,
      };
    case PROFESSIONAL_ADMIN_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };

    case GET_PLATFORM_MANAGER_PROFILE:
      return {
        ...state,
        platformManagerData: action.payload,
        loading: false,
      };
    case GET_PLATFORM_MANAGER_PROFILE_ERROR:
      return {
        ...state,
        loading: true,
      };
    case GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS:
      return {
        ...state,
        professionalAdminProfileDetails: action.payload,
        loading: false,
      };
    case GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

    case UPDATE_PROFESSIONAL_ADMIN_PROFILE:
      return {
        ...state,
        updateProfessionalAdminProfile: action.payload,
        loading: false,
      };
    case UPDATE_PROFESSIONAL_ADMIN_PROFILE_ERROR:
      return {
        ...state,
        loading: true,
      };

      case PLATFORM_MANAGER_UPDATE_PROFILE:
        return {
          ...state,
          platformManagerUpdate: action.payload,
          loading: false,
        };

        case  PLATFORM_MANAGER_UPDATE_PROFILE_ERROR:
      return {
        ...state,
        loading: true,
      };

      case PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST:
        return {
          ...state,
          plaformUserList: action.payload,
          loading: false,
        };

        case  PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST_ERROR:
      return {
        ...state,
        loading: true,
      };

      case PLATFORM_MANAGER_USER_PROFILE_DETAILS:
        return {
          ...state,
          platformUserDetails: action.payload,
          loading: false,
        };

        case  PLATFORM_MANAGER_USER_PROFILE_DETAILS_ERROR:
      return {
        ...state,
        loading: true,
      };

      case PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE:
        return {
          ...state,
          updateUserProfileDetails: action.payload,
          loading: false,
        };

        case   PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE_ERROR:
      return {
        ...state,
        loading: true,
      };

      default:
      return state;
  }
}
