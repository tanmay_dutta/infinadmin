import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";

import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getProfessionalAdminList } from "store/actions/platformManagerAction";
import LoadingScreen from "@/pages/loading";
import CsvDownload from "@/components/CsvDownload";
import PlatformProfAdminCard from "@/components/platform-manager-admin/PlatformProfAdminCard";
//Main
export default function PlatformAdmin() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProfessionalAdminList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformManagerReducer.getProfessionalAdminList
  );
  const professional_admin_List = result.content;


  const headers = [
    { label: "ID", key: "id" },
    { label: "FirstName", key: "firstname" },
    { label: "LasstName", key: "lastName" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];


  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />

          {/* Download list*/}
          <div className="top-filter-btns-group ms-auto d-flex flex-wrap">
           

            {professional_admin_List  ? (
                <CsvDownload
                  data={professional_admin_List}
                  headers={headers}
                  filename={"platform Manager Detail"}
                />
              ) : (
                ""
              )}
          </div>
          {professional_admin_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {professional_admin_List?.map((item, key) => {
                return (
                  //ProfessionalAdminCard
                  <PlatformProfAdminCard
                    id={item.id}
                    contactAddress={item.contactAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    professionalAdminDetailId={item.professionalAdminDetailId}
                    mobile={item.mobile}
                    membershipNumber={item.membershipNumber}
                    isVerified={item.isVerified}
                  
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
            </div>
          ) : (
            <LoadingScreen />
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
