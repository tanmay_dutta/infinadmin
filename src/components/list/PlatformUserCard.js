import { Avatar, Menu } from "@mantine/core";
import DownloadIcon from "@/components/icon/download";
import VerticalDot from "@/components/icon/vertical-dot";
import Edit from "../icon/edit";
import View from "../icon/view";
import Delete from "../icon/delete";
import CsvDownload from "../CsvDownload";
import { verifyProfessionalAdmin } from "store/actions/platformAdminAction";
import { useDispatch } from "react-redux";
import { deletePlatformUser } from "store/actions/platformAdminAction";

//main component for ProfessionalAdminCard
export default function PlatformUserCard(props) {
const dispatch = useDispatch();

  const headers = [
    { label: "firstName", key: "firstName" },
    { label: "lastName", key: "lastName" },
    { label: "Contact number", key: "mobile" },
    { label: "Email ID", key: "email" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "Valid Proof", key: "validIdProof" },
  ];
  let data = [
    {
      firstName: `${props.firstName} `,
      lastName: `${props.lastName}`,
      mobile: `${props.mobile}`,
      email: `${props.email}`,
      contactAddress: `${props.contactAddress}`,
      uploadedDocument: `${props.uploadedDocument}`,
      validIdProof: `${props.validIdProof}`,
    },
  ];

  return <div className="card admin-card-wedget">
    <div className="card-head">
      {/* Avatar card-image*/}
      <div className="card-image">
        <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
      </div>
       
     {/* name*/}
     <div className="info">
          <h3 className="card-title">{props.firstName} {" "} {props.lastName}</h3>
        </div>
    
      {/* Download */}
      <div
          className="btns ms-auto mb-auto d-flex flex-wrap">


              {props ? (
                <CsvDownload
                  data={data}
                  headers={headers}
                  filename={"platform User Detail"}
                />
              ) : (
                ""
              )}
        </div>
        { props.isVerified == 0?
        
        
        <div onClick={(e)=>{e.preventDefault();dispatch(verifyProfessionalAdmin(props.id))}} className="btn btn-sm btn-light-blue mb-auto d-flex flex-wrap">Verify</div>
      
       :'Verified'}

      {/* Menu Dropdown with VerticalDot*/}
      <Menu shadow="md" width={200} classNames="threedot-drops-menu">
        <Menu.Target>
          <div className="three-dot-btn"><VerticalDot /></div>
        </Menu.Target>
        <Menu.Dropdown>
          <Menu.Item component="a" href={`/platform-user/${props.id}`} icon={<View />} >View</Menu.Item>
          <Menu.Item component="a" href={`/platform-user-edit/${props.id}`} icon={<Edit />} >Edit</Menu.Item>
          <Menu.Item icon={<Delete />} onClick={(e)=>{e.preventDefault();dispatch(deletePlatformUser(props))}}>Delete</Menu.Item>
        </Menu.Dropdown>
      </Menu>
    </div>
    <div className="card-body">
      <div className="key-val-lists row">
       
        <div className="col-lg-8">
           {/* Contact number*/}
          <div className="item">
            <span className="key">Contact number: </span>
            <span className="val">{props.mobile}</span>
          </div>
          {/* Email ID*/}
        <div className="item col-lg-6">
          <span className="key">Email ID:  </span>
          <span className="val">{props.email}</span>
        </div>
        
        {/* Status*/}
        <div className="item">
          <span className="key">Status: </span>
          <span className="val"> FCA</span>
        </div>
        {/* Contact Address*/}
        <div className="item">
          <span className="key">Contact Address:  </span>
          <span className="val"> {props.contactAddress}</span>
        </div>
        </div>
       
        <div className=" col-lg-4">
          {/* Years of experience*/}
          <div className="item">
            <span className="key">Years of experience:  </span>
            <span className="val">10 years +</span>
          </div>
        {/* Subscribed plan*/}
        <div className="item">
          <span className="key">Subscribed plan:  </span>
          <span className="val"><span className="pill pill-light-blue">Professional</span></span>
        </div>
        </div>
        
        
        
        

      </div>
    </div>
  </div>
}