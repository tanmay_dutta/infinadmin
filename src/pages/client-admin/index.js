import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getProfessionalClientList } from "store/actions/platformAdminAction";
import ClientAdminList from "@/components/clientList/ClientAdminList";
import LoadingScreen from "../loading";
import CsvDownload from "@/components/CsvDownload";
//Main
export default function Home(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfessionalClientList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformAdminReducer.getClientAdminList
  );
  const client_Admin_List = result.content;

  const headers = [
    { label: "firstName", key: "firstName" },
    { label: "lastName", key: "lastName" },
    { label: "ID", key: "id" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];

  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />
          {/* Download  list*/}
          <div className="top-filter-btns-group ms-auto d-flex flex-wrap">
            {client_Admin_List ? (
              <CsvDownload
                data={client_Admin_List}
                headers={headers}
                filename={"Client Admin List"}
              />
            ) : (
              ""
            )}
          </div>

          {client_Admin_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {client_Admin_List?.map((item, key) => {
                return (
                  //ClientAdminList
                  <ClientAdminList
                    id={item.id}
                    communicationAddress={item.communicationAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    businessType={item.businessType}
                    companyName={item.companyName}
                    createdBy={item.createdBy}
                    gstNumber={item.gstNumber}
                    panNumber={item.panNumber}
                    mobile={item.mobile}
                    isVerified={item.isVerified}
                    clientAdminDetailId={item.clientAdminDetailId}
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
            </div>
          ) : (
            <LoadingScreen />
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
