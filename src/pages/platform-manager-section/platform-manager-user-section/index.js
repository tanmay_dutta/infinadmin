import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import UserDetailsCard from "@/components/platform-manager-admin/UserDetailsCard";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { platformManagerUserList } from "store/actions/platformManagerAction";
import LoadingScreen from "@/pages/loading";
import CsvDownload from "@/components/CsvDownload";
//Main
export default function PlatformAdmin() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(platformManagerUserList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformManagerReducer.plaformUserList
  );
  const platform_user_List = result.content;
  console.log("total list is",platform_user_List);

  //setting up download total list
  //const [userdata, setUserdata] = useState();
  // useEffect(() => {
  //   setUserdata(result.content);
  // }, []);

  const headers = [
    { label: "ID", key: "id" },
    { label: "Name", key: "name" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];

  // let data = [
  //   {
  //     id:`${professional_Admin_List.id}`,
  //     name: `${professional_Admin_List.firstName} ${professional_Admin_List.lastName}`,
  //     mobile: `${professional_Admin_List.mobile}`,
  //     email: `${professional_Admin_List.email}`,
  //     contactAddress: `${professional_Admin_List.contactAddress}`,
  //     platformManagerDetailId: `${professional_Admin_List.platformManagerDetailId}`,
  //     uploadedDocument: `${professional_Admin_List.uploadedDocument}`,
  //     validIdProof: `${professional_Admin_List.validIdProof}`,
  //   },
  // ];


  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />

          {/* Download list*/}
          {/* <div className="btns ms-auto mb-auto d-flex flex-wrap">
           

            {professional_Admin_List  ? (
                <CsvDownload
                  data={userdata}
                  headers={headers}
                  filename={"platform Manager Detail"}
                />
              ) : (
                ""
              )}
          </div> */}
          {platform_user_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {platform_user_List?.map((item, key) => {
                return (
                  //ProfessionalAdminCard
                  <UserDetailsCard
                    id={item.id}
                    contactAddress={item.contactAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    platformUserDetailId={item.platformUserDetailId}
                    mobile={item.mobile}
                    uploadedDocument={item.uploadedDocument}
                    validIdProof={item.validIdProof}
                    platformManagerId = {item.platformManagerId}
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
            </div>
          ) : (
            <LoadingScreen />
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
