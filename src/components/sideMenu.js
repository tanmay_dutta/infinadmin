import { Box, NavLink } from "@mantine/core";
import Dashboard from "@/components/icon/dashboard";
import Router from "next/router";
import { useRouter } from "next/router";
import Image from "next/image";
import Bag from "./icon/bag";
import { toast } from "react-toastify";
import IconLogout from "./icon/IconLogout";
import IconClientUser from "./icon/IconClientUser";
import IconClientAdmin from "./icon/IconClientAdmin";
import IconProfUser from "./icon/IconProfUser";
import IconManager from "./icon/IconManager";
import { useEffect, useState } from "react";

//main component for SideMenu
export default function SideMenu() {
  let  [roles, setRoles] = useState('')


  useEffect(() => {
    if (typeof window !== 'undefined') {
      // Perform localStorage action
      setRoles(localStorage.getItem('userroles'));
    }

  }, [roles])



  const logOut = () => {
    toast.success("Logged out Successfully");
    localStorage.clear();
    Router.push("/"); //routing to home component
  };

  function goto(path){
    Router.push(path);
  }
  function activeMenu(path, isparent=null, parent_path){
      if(router.pathname === path){ 
        return true;
    }

    if(isparent){
      let get_path_array = router.pathname.split("/");
      if(get_path_array.indexOf(parent_path) !== -1 ){
        return true;
      }
      return false;
    }
  }

  const router = useRouter();
  console.log("rounter", router.pathname);

  const platformCall = () =>{
    console.log("platform call");
  }
  return (
    <Box className="admin-sidebar">
      
      {/* logo img */}
      <div className="brand-logo">
        <Image src="/logo.png" alt="" width={100} height={45}/>
      </div>
      
      {
      (roles==="ROLE_PLATFORM_ADMIN")?
      <div className="admin-menus">
         {/* platform-admin */}
        <NavLink
          active={activeMenu('/platform-admin')}
          className={`${activeMenu('/platform-admin') == true ? 'active' : ''}`}
          label="Dashboard"
          icon={<Dashboard color="#000" size={25} />}
          onClick={() => {
            goto('/platform-admin');
          }}
        />
         {/* plateformadmin profile details */}
        <NavLink
          className={`${activeMenu('/platform-admin/profileDetails') == true ? 'active' : ''}`}
          label="Platform Admin Profile"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
           // toast.success("Welcome To Platform Admin Profile")
            Router.push("/platform-admin/profileDetails");
          }}
        />

          <NavLink label="Platform Manager" onClick={() =>{ 
            //toast.success("Welcome To Platform Manager Section")
            Router.push("/platform-manager")}} icon={<IconManager color="#000"/>} 
            className={`${activeMenu('/platform-manager',true, 'platform-manager') == true ? 'active' : ''}`}
            defaultOpened={activeMenu('/platform-manager', true, 'platform-manager')}
            >
            <NavLink
              className={`${activeMenu('/platform-manager/create') == true ? 'active' : ''}`}
              label="Create"
              icon={<IconManager color="#000" />}
              onClick={() => Router.push("/platform-manager/create")}
            ></NavLink>
          </NavLink>
        <NavLink
        className={`${activeMenu('/professional-admin') == true ? 'active' : ''}`}
          label="Professional Admin"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
            //toast.success("Welcome To Professional Admin Section")
            Router.push("/professional-admin");
          }}
        />
         
        <NavLink label="Platform User" onClick={() => Router.push("/platform-user")} icon={<IconProfUser color="#000"/>}
        className={`${activeMenu('/platform-user', true, 'platform-user') == true ? 'active' : ''}`}
        defaultOpened = {activeMenu('/platform-user', true, 'platform-user')}
        >
            <NavLink
              label="Create"
              icon={<IconProfUser color="#000" />}
              className={`${activeMenu('/platform-user/create') == true ? 'active' : ''}`}
              onClick={() => Router.push("/platform-user/create")}
            ></NavLink>
          </NavLink>
       
         {/* Client Admin */}
        <NavLink
        className={`${activeMenu('/client-admin') == true ? 'active' : ''}`}
          label="Client Admin"
          icon={<IconClientAdmin color="#000" />}
          onClick={() => Router.push("/client-admin")}
        />
         {/* Client User */}
        <NavLink
          className={`${activeMenu('/client-user') == true ? 'active' : ''}`}
          label="Client User"
          icon={<IconClientUser />}
          onClick={() => Router.push("/comingSoon")}
        />
        
      </div>:
      
      (roles==="ROLE_PLATFORM_MANAGER")?
      <div className="admin-menus">
        <NavLink
          active={activeMenu('/platform-manager-section')}
          className={`${activeMenu('/platform-manager-section') == true ? 'active' : ''}`}
          label="Dashboard"
          icon={<Dashboard color="#000" size={25} />}
          onClick={() => {
            goto('/platform-manager-section');
          }}
        />
        <NavLink
          className={`${activeMenu('/platform-manager-section/profileDetails') == true ? 'active' : ''}`}
          label="Profile"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
           // toast.success("Welcome To Platform Admin Profile")
            Router.push("/platform-manager-section/profileDetails");
          }}
        />

     <NavLink
          className={`${activeMenu('/platform-manager-section/professional-admin') == true ? 'active' : ''}`}
          label="Professional Admin"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
           // toast.success("Welcome To Platform Admin Profile")
            Router.push("/platform-manager-section/professional-admin");
          }}
        />

<NavLink
          className={`${activeMenu('/platform-manager-section/platform-manager-user-section') == true ? 'active' : ''}`}
          label="Platform User"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
           // toast.success("Welcome To Platform Admin Profile")
            Router.push("/platform-manager-section/platform-manager-user-section");
          }}
        />
        <NavLink
          className={`${activeMenu('/client-user') == true ? 'active' : ''}`}
          label="Client User"
          icon={<IconClientUser />}
          onClick={() => Router.push("/comingSoon")}
        />
        
      </div>:
      
      (roles==="ROLE_PLATFORM_USER")?
      <div className="admin-menus">
         {/* platform-admin */}
        <NavLink
          active={activeMenu('/platform-user-section')}
          className={`${activeMenu('/platform-user-section') == true ? 'active' : ''}`}
          label="Dashboard"
          icon={<Dashboard color="#000" size={25} />}
          onClick={() => {
            goto('/platform-user-section');
          }}
        />
         {/* plateformadmin profile details */}
        <NavLink
          className={`${activeMenu('/platform-user-section/profileDetails') == true ? 'active' : ''}`}
          label="Platform User Profile"
          icon={<IconProfUser color="#000"/>}
          onClick={() => {
           // toast.success("Welcome To Platform Admin Profile")
            Router.push("/platform-user-section/profileDetails");
          }}
        />


        <NavLink
        className={`${activeMenu('/professional-admin') == true ? 'active' : ''}`}
          label="Professional Admin"
          icon={<Bag color="#000" size={25} />}
          onClick={() => {
            //toast.success("Welcome To Professional Admin Section")
            Router.push("/professional-admin");
          }}
        />
         
       
        
      </div>:
      <div className="admin-menus">
        
       
       
       <NavLink
         className={`${activeMenu('/client-user') == true ? 'active' : ''}`}
         label="Client Admin"
         icon={<IconClientUser />}
         onClick={() => Router.push("/comingSoon")}
       />
       
     </div>}


       {/* Logout*/}
      <div className="admin-menu-bottom">
        <NavLink
          label="Logout"
          component="a"
          icon={<IconLogout color="currentcolor" />}
          onClick={() => logOut()}
        />
      </div>
    </Box>
  );
}
