/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    //NEXT_PUBLIC_API_URL_NEW: "http://128.199.30.38:5000/" , //Dev URL
    NEXT_PUBLIC_API_URL: "https://infin.mydevfactory.com:8443/"  //Prod URL

  },
}

module.exports = nextConfig
