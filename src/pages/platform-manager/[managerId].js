import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { platformManqgerProfileDetails } from "store/actions/platformAdminAction";
import { useRouter } from "next/router";
import LoadingScreen from "../loading";

export default function ManagerDetails() {
  const router = useRouter();
  const managerId = router.query.managerId;
  const dispatch = useDispatch();
  useEffect(() => {
    if (managerId) {
     // toast.success("Welcome");
      dispatch(platformManqgerProfileDetails(managerId));
    }
  }, [managerId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.platformManagerProfileDetails
  );



  
  
  return (
    <>

    

      <div className="admin_wrap container-fluid">
        {/*SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />
          {result.id === undefined ?<LoadingScreen />:
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                {/* Name*/}
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">{result.firstName}{" "}{result.lastName}</span>
                </div>
                {/* Contact Number*/}
                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                {/* Email address*/}
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                {/* Platform Manager Detail ID*/}
                <div className="item">
                  <span className="key">Platform Manager Detail ID: </span>
                  <span className="val"> {result.platformManagerDetailId}</span>
                </div>
                {/* Uploaded Document*/}
                <div className="item">
                  <span className="key">Uploaded Document: </span>
                  <span className="val">{result.uploadedDocument}</span>
                </div>
                {/* Contact address*/}
                <div className="item">
                  <span className="key">Contact address: </span>
                  <span className="val">{result.contactAddress}</span>
                </div>
                {/* Valid Proof*/}
                <div className="item">
                  <span className="key">Valid Proof: </span>
                  <span className="val">{result.validIdProof}</span>
                </div>
              </div>
            </div>
          </div>
}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
