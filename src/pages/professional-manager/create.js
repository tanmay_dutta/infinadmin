import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import ProfessionalManagerCard from "@/components/professional-manager-card";
import CreateManager from "@/components/professional-manager-card/create-manager";
import SideMenu from "@/components/sideMenu";

// main 
export default function ProfessionalManagerCreate() {
    return <>
    <div className='admin_wrap container-fluid'>
      {/* SideMenu*/}
          <SideMenu />  
          <div className="admin-content-area">
            {/* AdminHeader*/}
            <AdminHeader />  
            <div className="dashboard-widget-wrap">
              {/* CreateManager*/}
              <CreateManager />
            </div>
          </div>
        </div>
        <Footer />
    </>
  }