import { Button, NativeSelect, TextInput } from "@mantine/core";
import HomeIcon from "@/components/icon/HomeIcon";
import { Dropzone } from "@mantine/dropzone";
import { useRef,useState  } from "react";
import { useForm } from "@mantine/form";
import { createPlatformManager } from "store/actions/platformAdminAction";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import Link from "next/link";

//main component for CreatePlatformManager
export default function CreatePlatformManager() {
  const selectedFile = [];
  const dispatch = useDispatch();
  const router = useRouter();
  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      mobile: "",
      contactAddress: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  const openRef = useRef(null);
  return (
    <div className="card admin-card-wedget">
      <form
        onSubmit={form.onSubmit((values) =>

          {
          dispatch(createPlatformManager(values)) //action calling
          // values.file.push(selectedFile)
          console.log(values, selectedFile, "=======");
          }
        )}
      >
        <div className="card-body p-0">
          <div className="row side-gap-0">
             {/* First name*/}
            <div className="col-lg-6">
              <TextInput placeholder="First name" label="First name" {...form.getInputProps("firstName")}/>
            </div>
             {/* Last name*/}
            <div className="col-lg-6">
              <TextInput placeholder="Last name" label="Last name" {...form.getInputProps("lastName")}/>
            </div>
             {/* Email address*/}
            <div className="col-lg-6">
              <TextInput placeholder="Email address" label="Email address" {...form.getInputProps("email")}/>
            </div>
             {/* Contact number*/}
            <div className="col-lg-6">
              <TextInput placeholder="Contact number" label="Contact number" {...form.getInputProps("mobile")}/>
            </div>
             {/* Password*/}
            <div className="col-lg-12">
              <TextInput placeholder="Password" label="Password" {...form.getInputProps("password")}/>
            </div>
             {/* Location*/}
            <div className="col-lg-12">
              <TextInput placeholder="Location" label="Location" {...form.getInputProps("contactAddress")}/>
            </div>
            
            {/* <div className="col-lg-12">
              <TextInput placeholder="platform-manager" disabled/>
            </div> */}
             {/* Dropzone*/}
            <div className="col-lg-12">
              <Dropzone
                openRef={openRef}
                onDrop={(files) => {
                  console.log("---===",files);
                  console.log("===---",files[0]);
                  // console.log("accepted files", files);
                 // selectedFile.push(files);
                }}
                // onDrop={handleFileDrop}
                activateOnClick={false}
                className="file-select-input"
                styles={{ inner: { pointerEvents: "all" } }}
              >
                 {/* button*/}
                <button
                  onClick={() => openRef.current()}
                  className="file-load-btn"
                >
                  <span className="icon">
                    <svg
                      width="34"
                      height="34"
                      viewBox="0 0 34 34"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M16.9669 -7.60645e-08C17.9279 -3.40552e-08 18.707 0.779091 18.707 1.74015L18.707 32.1927C18.707 33.1538 17.9279 33.9329 16.9669 33.9329C16.0058 33.9329 15.2267 33.1538 15.2267 32.1927L15.2267 1.74015C15.2267 0.779091 16.0058 -1.18074e-07 16.9669 -7.60645e-08Z"
                        fill="#47B6F4"
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M0 17.8368C0 16.8758 0.779091 16.0967 1.74015 16.0967L32.1927 16.0967C33.1538 16.0967 33.9329 16.8758 33.9329 17.8368C33.9329 18.7979 33.1538 19.577 32.1927 19.577L1.74015 19.577C0.779091 19.577 0 18.7979 0 17.8368Z"
                        fill="#47B6F4"
                      />
                    </svg>
                  </span>
                  Upload Documents/ID
                </button>
              </Dropzone>
            </div>
            {/* <div className="col-lg-12">
              <NativeSelect
                data={[
                  "Assign client/Task",
                  "submenu 1",
                  "submenu 2",
                  "submenu 3",
                ]}
                withAsterisk
              />
            </div> */}
             {/* Create Manager Button*/}
            <div className="col-lg-12 actions place-center mt-5">
              <Button type="submit">Create </Button>
              <Link href="/platform-admin" className="btn btn-primary-light"><span className="icon me-2"><HomeIcon size={20} /></span>Back to Home</Link>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
