import { CSVLink } from "react-csv";
import DownloadIcon from "./icon/download";
export default function CsvDownload({ data, headers ,filename}) {
    return (<>
        <CSVLink className="btn btn-sm btn-light-blue" data={data} headers={headers} filename={`${filename}.csv`}
        >
         <span className="icon"><DownloadIcon/></span>
         {
            data.length == 1 ? 'Download Details' : 'Download All'
         } 
         
        </CSVLink>
    </>)
}