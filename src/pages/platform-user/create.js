import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import CreatePlatformUser from "@/components/platform-user/create-user";
import SideMenu from "@/components/sideMenu";


export default function PlatformUserCreate() {
    return <>
    <div className='admin_wrap container-fluid'>
          <SideMenu />
          <div className="admin-content-area">
            <AdminHeader />
            <div className="dashboard-widget-wrap">
              <CreatePlatformUser />
            </div>
          </div>
        </div>
        <Footer />
    </>
  }