import{
    PROFESSIONAL_ADMIN_LIST,
    PROFESSIONAL_ADMIN_LIST_ERROR,
    GET_PLATFORM_MANAGER_PROFILE,
    GET_PLATFORM_MANAGER_PROFILE_ERROR,
    PLATFORM_MANAGER_UPDATE_PROFILE,
    PLATFORM_MANAGER_UPDATE_PROFILE_ERROR,
    PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST,
    PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST_ERROR,
    PLATFORM_MANAGER_USER_PROFILE_DETAILS,
    PLATFORM_MANAGER_USER_PROFILE_DETAILS_ERROR,
    PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE,
    PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE_ERROR,
    GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
    GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
    UPDATE_PROFESSIONAL_ADMIN_PROFILE ,
    UPDATE_PROFESSIONAL_ADMIN_PROFILE_ERROR


}  from "../types";
import axios from "axios";
import Router from "next/router";
import { toast } from "react-toastify";



//Action dispatcher for getting profetional admin list

export const getProfessionalAdminList = (e) => async (dispatch) => {
    let token = localStorage.getItem("token_key");
    try {
      var config = {
        method: "GET",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/professional-admin`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
      };
  
      const res = await axios(config);
  
      if (res.data) {
        console.log("data is", res.data);
        dispatch({
          type: PROFESSIONAL_ADMIN_LIST,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: PROFESSIONAL_ADMIN_LIST_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  };



  //Action dispatcher for getting platform manager profile
export const platformManagerProfileDetails = () => async (dispatch) => {
    let token = localStorage.getItem("token_key");
    
    try {
      var config = {
        method: "GET",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/profile`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
      };
  
      const res = await axios(config);
      if (res.data) {
        console.log("platform manager details", res.data);
        dispatch({
          type: GET_PLATFORM_MANAGER_PROFILE,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: GET_PLATFORM_MANAGER_PROFILE_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  };

  //platform manager update profile
  export const platformManagerUpdateProfileDetails = (e) => async (dispatch) => {
    let token = localStorage.getItem("token_key");
    let managerId = localStorage.getItem("id");
    let platformManagerDetail = {
      platformManagerDetailId: managerId,
      contactAddress: e.contactAddress,
      uploadedDocument: "kkkkkk",
      validIdProof: "kkkkkk",
    };
    let data = JSON.stringify({
      firstName: e.firstName,
      lastName: e.lastName,
      mobile: e.mobile,
      platformManagerDetail: platformManagerDetail,
    });
    try {
      var config = {
        method: "POST",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/update-profile`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
        data: data,
      };
  
      const res = await axios(config);
      if (res.data) {
        dispatch({
          type: PLATFORM_MANAGER_UPDATE_PROFILE,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: PLATFORM_MANAGER_UPDATE_PROFILE_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  };

  export const platformManagerUserList = () => async (dispatch) => {
    let token = localStorage.getItem("token_key");
    
    try {
      var config = {
        method: "GET",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/platform-user`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
      };
  
      const res = await axios(config);
  console.log("data from action",res)
      if (res.data) {
        if(res.data.page === 0){
          toast.success("platform user list is empty will be updated soon");
        }
        dispatch({
          type: PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: PLATFORM_MANAGER_SECTION_PLATFORM_USER_LIST_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  };

  export const platformManagerUserProfileDetails = (id) => async (dispatch) => {
    let token = localStorage.getItem("token_key");
  
    try {
      var config = {
        method: "GET",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/platform-user-profile-detail/${id}`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
      };
  
      const res = await axios(config);
      if (res.data) {
        dispatch({
          type: PLATFORM_MANAGER_USER_PROFILE_DETAILS,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: PLATFORM_MANAGER_USER_PROFILE_DETAILS_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  };

  export const updatePlatformUserProfileDetails = (e) =>  async (dispatch) => {
    let token = localStorage.getItem("token_key");
    let managerId = localStorage.getItem("id");
    let platformManagerDetail = {
      platformUserDetailId: e.id,
      platformManagerDetailId: managerId,
      contactAddress: e.contactAddress,
      uploadedDocument: "kkkkkk",
      validIdProof: "kkkkkk",
    };
    let data = JSON.stringify({
      firstName: e.firstName,
      lastName: e.lastName,
      mobile: e.mobile,
      platformManagerDetail: platformManagerDetail,
    });
    try {
      var config = {
        method: "PUT",
        url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/update-platform-user-profile/${e.id}`,
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
          Authorization: `Bearer ${token}`,
        },
        data:data,
      };
  
      const res = await axios(config);
      if (res.data) {
        dispatch({
          type: PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE,
          payload: res.data,
        });
      }
    } catch (error) {
      dispatch({
        type: PLATFORM_MANAGER_USER_PROFILE_DETAILS_UPDATE_ERROR,
        payload: error?.response?.error?.message,
      });
    }
  }


    //Action dispatcher for getting professional admin profile details
    export const professionalAdminProfileDetailstest = (id)=> async (dispatch) => {
       let token = localStorage.getItem("token_key");
     
       try {
         var config = {
           method: "GET",
           url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/professional-admin-profile-detail/${id}`,
           headers: {
             "Content-Type": "application/json",
             // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
             Authorization: `Bearer ${token}`,
           },
         };
     
         const res = await axios(config);
         if (res.data) {
           dispatch({
             type: GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
             payload: res.data,
           });
         }
       } catch (error) {
         dispatch({
           type: GET_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
           payload: error?.response?.error?.message,
         });
       }
     };
   
   
     //Action dispatcher for update professional admin profile profile
   export const updateProfessionalAdminProfile = (e) => async (dispatch) => {
     let token = localStorage.getItem("token_key");
     console.log("professional admin id", e.id);
   
     let adminRequest = {
       professionalAdminDetailId: e.id,
       membershipNumber: "77777",
       contactAddress: e.contactAddress,
     };
     let data = JSON.stringify({
       firstName: e.firstName,
       lastName: e.lastName,
       mobile: e.mobile,
       professionalAdminDetail: adminRequest,
     });
     //let id = e.id;
     try {
       var config = {
         method: "PUT",
         url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-manager/update-professional-admin-profile/${e.id}`,
   
         headers: {
           "Content-Type": "application/json",
           // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
           Authorization: `Bearer ${token}`,
         },
         data: data,
       };
   
       const res = await axios(config);
       console.log("hi", res.data);
       if (res.data) {
         console.log("response iss", res.data);
         // toast.success("created successfully", {
         //   onClose: () => Router.push("/platform-manager"),
         // });
         if (res.data.success) {
           toast.success(res.data.message);
   
          // Router.push("/professional-admin");
         } else {
           toast.error(res.response.data.message);
         }
   
         dispatch({
           type:  UPDATE_PROFESSIONAL_ADMIN_PROFILE,
           payload: res.data,
         });
       }
     } catch (error) {
       console.log("error is", error.response.data);
       toast.error(error.response.data.message);
       dispatch({
         type: UPDATE_PROFESSIONAL_ADMIN_PROFILE_ERROR,
         payload: error?.response?.error?.message,
       });
     }
   };


  