import { Avatar, Menu } from "@mantine/core";
import DownloadIcon from "@/components/icon/download";
import VerticalDot from "@/components/icon/vertical-dot";
import Edit from "../icon/edit";
import Delete from "../icon/delete";
import View from "../icon/view";
import CsvDownload from "../CsvDownload";
import { verifyProfessionalAdmin } from "store/actions/platformAdminAction";
import { deletePlatformManager } from "store/actions/platformAdminAction";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";


//main component for ProfessionalManegerCard
export default function PlatformManagerCard(props) {
  const dispatch = useDispatch()
  const router = useRouter();
  const headers = [
    { label: "Name", key: "name" },
    { label: "Contact number", key: "mobile" },
    { label: "Email ID", key: "email" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "Valid Proof", key: "validIdProof" },
  ];
  let data = [
    {
      name: `${props.firstName} ${props.lastName}`,
      mobile: `${props.mobile}`,
      email: `${props.email}`,
      contactAddress: `${props.contactAddress}`,
      uploadedDocument: `${props.uploadedDocument}`,
      validIdProof: `${props.validIdProof}`,
    },
  ];
  return (
    <div className="card admin-card-wedget">
      <div className="card-head">
        {/* Avatar card-image*/}
        <div className="card-image">
          <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
        </div>
        {/* Serial Number*/}
        <div className="info">
          <h3 className="card-title">{props.firstName} {props.lastName}</h3>
        </div>
        {/* Download */}
        <div
          className="btns ms-auto mb-auto d-flex flex-wrap">


              {props ? (
                <CsvDownload
                  data={data}
                  headers={headers}
                  filename={"platform Manager Detail"}
                />
              ) : (
                ""
              )}
        </div>
        { props.isVerified == 0?
        
        
        <div onClick={(e)=>{e.preventDefault();dispatch(verifyProfessionalAdmin(props.id))}} className="btn btn-sm btn-light-blue mb-auto d-flex flex-wrap">Verify</div>
      
       :'Verified'}
        
        {/* Menu Dropdown with VerticalDot*/}
        <Menu shadow="md" width={200} classNames="threedot-drops-menu">
          <Menu.Target>
            <div className="three-dot-btn">
              <VerticalDot />
            </div>
          </Menu.Target>
          <Menu.Dropdown>
            <Menu.Item
              component="a"
              href={`/platform-manager/${props.id}`}
              icon={<View />}
            >
              {" "}
              View
            </Menu.Item>
            <Menu.Item
              icon={<Edit />}
              component="a"
              href={`/platform-manager-edit/${props.id}`}
            >
              Edit
            </Menu.Item>
            <Menu.Item icon={<Delete />} onClick={(e)=>{e.preventDefault();dispatch(deletePlatformManager(props))}} >
              Delete
            </Menu.Item>
          </Menu.Dropdown>
        </Menu>
      </div>
      <div className="card-body">
        <div className="key-val-lists row">
          
          {/* Contact number*/}
          <div className="col-lg-8">

       

            <div className="item">
              <span className="key">Contact number: </span>
              <span className="val">{props.mobile}</span>
            </div>
            {/* Email ID*/}
            <div className="item">
              <span className="key">Email ID: </span>
              <span className="val text-truncate">{props.email}</span>
            </div>

            {/* Status*/}
            <div className="item">
              <span className="key">Status: </span>
              <span className="val"> FCA</span>
            </div>
            {/* Contact Address*/}
            <div className="item">
              <span className="key">Contact Address: </span>
              <span className="val"> {props.contactAddress}</span>
            </div>

            {/* Uploaded Document*/}
            <div className="item">
              <span className="key">Uploaded Document: </span>
              <span className="val"> {props.uploadedDocument}</span>
            </div>
            {/* Valid Proof*/}
            <div className="item">
              <span className="key">Valid Proof: </span>
              <span className="val"> {props.validIdProof}</span>
            </div>
          </div>
          {/* Years of experience*/}
          <div className=" col-lg-4">
            <div className="item">
              <span className="key">Years of experience: </span>
              <span className="val">10 years +</span>
            </div>
            {/* Subscribed plan*/}
            <div className="item">
              <span className="key">Subscribed plan: </span>
              <span className="val">
                <span className="pill pill-light-blue small">Professional</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
