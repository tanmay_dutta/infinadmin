import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { platformManagerUserProfileDetails } from "store/actions/platformManagerAction";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

export default function ProfileDetails() {
  const dispatch = useDispatch();
  let router = useRouter();
  const detailsId = router.query.detailsId;
  console.log("professional admin id: ", detailsId);
  useEffect(() => {
    if (detailsId) {
      dispatch(platformManagerUserProfileDetails(detailsId)); // action calling
    }
  }, [detailsId, dispatch]);

  const result = useSelector(
    (state) => state.platformManagerReducer.platformUserDetails
  );
  console.log("result of user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">
                    {result.firstName} {result.lastName}
                  </span>
                </div>

                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                <div className="item">
                  <span className="key">Contact Address: </span>
                  <span className="val">{result.contactAddress}</span>
                </div>
                <div className="item">
                  <span className="key">Platform Manager Detail ID: </span>
                  <span className="val">{result.platformManagerDetailId}</span>
                </div>
                <div className="item">
                  <span className="key">Uploaded Document: </span>
                  <span className="val">{result.uploadedDocumen}</span>
                </div>
                <div className="item">
                  <span className="key">Valid Proof: </span>
                  <span className="val">{result.validIdProof}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
