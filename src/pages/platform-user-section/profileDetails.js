import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function ProfileDetails() {
  // const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(platformManagerProfileDetails());
  // }, [dispatch]);

  // const result = useSelector((state) => state.platformManagerReducer.platformManagerData);
  // console.log("result is", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">Sanghamita Paul</span>
                </div>

                <div className="item">
                  <span className="key">Client Name: </span>
                  <span className="val">Cleint User</span>
                </div>

                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">9903577862</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val">platformUser@gmail.com</span>
                </div>
                <div className="item">
                  <span className="key">Contact Address: </span>
                  <span className="val">Tarapith</span>
                </div>

                <div className="item">
                  <span className="key">Subscribed plan: </span>
                  <span className="val">
                    <span className="pill pill-light-blue small">
                      Professional
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
