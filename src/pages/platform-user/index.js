import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import PlatformUserCard from "@/components/list/PlatformUserCard";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { platformuserList } from "store/actions/platformAdminAction";
import LoadingScreen from "../loading";
import CsvDownload from "@/components/CsvDownload";
//Main
export default function PlatformUser(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(platformuserList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformAdminReducer.platformUserList
  );
  const Platform_User_List = result.content;

  const headers = [
    { label: "ID", key: "id" },
    { label: "firstName", key: "firstName" },
    { label: "lastName", key: "lastName" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];

  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />
          {/* Download */}
          <div className="top-filter-btns-group ms-auto d-flex flex-wrap">
            {Platform_User_List ? (
              <CsvDownload
                data={Platform_User_List}
                headers={headers}
                filename={"Platform user List"}
              />
            ) : (
              ""
            )}
          </div>

          {Platform_User_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {Platform_User_List?.map((item, key) => {
                return (
                  //ProfessionalAdminCard
                  <PlatformUserCard
                    id={item.id}
                    contactAddress={item.contactAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    platformManagerDetailId={item.platformManagerDetailId}
                    mobile={item.mobile}
                    uploadedDocument={item.uploadedDocument}
                    validIdProof={item.validIdProof}
                    platformUserDetailId={item.platformUserDetailId}
                    isVerified={item.isVerified}
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
            </div>
          ) : (
            <LoadingScreen />
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
