import {
  AUTH_LOGIN_START,
  AUTH_LOGIN,
  AUTH_ERROR,
  PLATFORM_ADMIN_DETAILS,
  PLATFORM_ADMIN_DETAILS_ERROR,
  GET_PROFESSIONAL_ADMIN_LIST,
  GET_PROFESSIONAL_ADMIN_LIST_ERROR,
  GET_CLIENT_ADMIN_LIST,
  GET_CLIENT_ADMIN_LIST_ERROR,
  UPDATE_PLATFORM_PROFILE_DETAILS,
  UPDATE_PLATFORM_PROFILE_DETAILS_ERROR,
  PLATFORM_USER_PROFILE_DETAILS,
  PLATFORM_USER_PROFILE_DETAILS_ERROR,
  PROFESSIONAL_ADMIN_PROFILE_DETAILS,
  PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
  CLIENT_ADMIN_PROFILE_DETAILS,
  CLIENT_ADMIN_PROFILE_DETAILS_ERROR,
  PLATFORM_MANAGER_PROFILE_DETAILS,
  PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
  CREATE_PLATFORM_USER,
  CREATE_PLATFORM_USER_ERROR,
  CREATE_PLATFORM_MANAGER,
  CREATE_PLATFORM_MANAGER_ERROR,
  PLATFORM_MANAGER_LIST,
  PLATFORM_MANAGER_LIST_ERROR,
  PLATFORM_USER_LIST,
  PLATFORM_USER_LIST_ERROR,
  FORGET_PASSWORD,
  FORGET_PASSWORD_ERROR,
  VERIFY_TOKEN,
  VERIFY_TOKEN_ERROR,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
  UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
  UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS,
  UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
  VERIFY_PROFESSIONAL_ADMIN,
  VERIFY_PROFESSIONAL_ADMIN_ERROR,
  UPDATE_PLATFORM_USER,
  UPDATE_PLATFORM_USER_ERROR,
  UPDATE_CLIENT_ADMIN,
  UPDATE_CLIENT_ADMIN_ERROR,
  DELTE_PLATFORM_MANAGER,
  DELTE_PLATFORM_MANAGER_ERROR,
  DELETE_PLATFORM_USER,
  DELETE_PLATFORM_USER_ERROR,
  DELETE_PROFESSIONAL_ADMIN,
  DELETE_PROFESSIONAL_ADMIN_ERROR,
  DELTE_CLIENT_ADMIN,
  DELTE_CLIENT_ADMIN_ERROR,
} from "../types";
import axios from "axios";
import Router from "next/router";
import { toast } from "react-toastify";
//import { useRouter } from "next/router";

//Action Dispatcher for Admin login
export const handleLogin = (e) => async (dispatch) => {
  dispatch({
    type: AUTH_LOGIN_START,
  });

  try {
    let data = {
      email: e.email,
      password: e.password,
    };
    var config = {
      method: "post",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/signin`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    const res = await axios(config);

    toast.success(res.data.message);
    if (res.data.success === false) {
      toast.success(res.data.message);
    } else {
      localStorage.setItem("token_key", res.data.accessToken);
      localStorage.setItem("id", res.data.id);
      localStorage.setItem("firstname", res.data.firstName);
      localStorage.setItem("lastname", res.data.lastName);
      localStorage.setItem("userroles", res.data.roles);

      if (res.data.roles[0] === "ROLE_PLATFORM_ADMIN") {
        Router.push("/platform-admin");
      } else if (res.data.roles[0] === "ROLE_PLATFORM_MANAGER") {
        Router.push("/platform-manager-section");
      } else if (res.data.roles[0] === "ROLE_PLATFORM_USER") {
        Router.push("/platform-user-section");
      } else {
        Router.push("/comingSoon");

      }

      dispatch({
        type: AUTH_LOGIN,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error("Unable to Sign in");
    dispatch({
      type: AUTH_ERROR,
      payload: error.response,
    });
  }
};

//Action dispatcher function for platfrom admin profile details

export const platFormAdminProfileDetails = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/platform-admin-profile-detail`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      // toast.success("Welcome To Platform Admin Profile")
      dispatch({
        type: PLATFORM_ADMIN_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PLATFORM_ADMIN_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for getting profetional admin list

export const getProfessionalAdminList = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
 
  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/professional-admin`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);

    if (res.data) {
      dispatch({
        type: GET_PROFESSIONAL_ADMIN_LIST,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_PROFESSIONAL_ADMIN_LIST_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for getting professional client admin list
export const getProfessionalClientList = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/client-admin`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      dispatch({
        type: GET_CLIENT_ADMIN_LIST,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_CLIENT_ADMIN_LIST_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const platformManagerList = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/platform-manager`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);

    if (res.data) {
      dispatch({
        type: PLATFORM_MANAGER_LIST,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PLATFORM_MANAGER_LIST_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const platformuserList = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/platform-user`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      dispatch({
        type: PLATFORM_USER_LIST,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PLATFORM_USER_LIST_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const updatePlatformAdminProfileDetails = (e) => async (dispatch) => {
  //const router=useRouter();
  let token = localStorage.getItem("token_key");
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    mobile: e.mobile,
  });
  try {
    var config = {
      method: "POST",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/update-profile`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);

    if (res.data) {
      if (res.data.success) {
        toast.success(res.data.message);
        localStorage.setItem("firstname", e.firstName);
        localStorage.setItem("lastname", e.lastName);
        Router.push("/platform-admin");
      } else {
        toast.error(res.data.message);
      }

      dispatch({
        type: UPDATE_PLATFORM_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: UPDATE_PLATFORM_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const platformUserProfileDetails = (id) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/platform-user-profile-detail/${id}`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      dispatch({
        type: PLATFORM_USER_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PLATFORM_USER_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//done
export const professionalAdminProfileDetails = (id) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/professional-admin-profile-detail/${id}`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      dispatch({
        type: PROFESSIONAL_ADMIN_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const clientlAdminProfileDetails = (id) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/client-admin-profile-detail/${id}`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);
    if (res.data) {
      dispatch({
        type: CLIENT_ADMIN_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: CLIENT_ADMIN_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//working on
export const platformManqgerProfileDetails = (id) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  try {
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/platform-manaer-profile-detail/${id}`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
    };

    const res = await axios(config);

    if (res.data) {
      // toast.success("Welcome")
      dispatch({
        type: PLATFORM_MANAGER_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for create plateform user
export const createPlatformUser = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  let address = e.contactAddress;

  // let document = e.uploadedDocument;
  // let proof = e.validIdProof;
  let managerId = localStorage.getItem("id");
  let userRequest = {
    contactAddress: address,
    uploadedDocument: "kkkkkk",
    validIdProof: "kkkkkk",
    platformManagerId: managerId,
  };
  let data = {
    firstName: e.firstName,
    lastName: e.lastName,
    email: e.email,
    password: e.password,
    mobile: e.mobile,
    role: 3,
    platformUserRequest: userRequest,
  };

  try {
    var config = {
      method: "POST",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/create-platform-user`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);
    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-user"),
      // });
      toast.success("New Platform User is Created  ");

      dispatch({
        type: CREATE_PLATFORM_USER,
        payload: res.data,
      });

      Router.push("/platform-user");
    }
  } catch (error) {
    dispatch({
      type: CREATE_PLATFORM_USER_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for create plateform manager
export const createPlatformManager = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  let address = e.contactAddress;

  // let document = e.uploadedDocument;
  // let proof = e.validIdProof;
  //let managerId = localStorage.getItem("id");
  let managerRequest = {
    contactAddress: address,
    uploadedDocument: "kkkkkk",
    validIdProof: "kkkkkk",
  };
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    email: e.email,
    password: e.password,
    mobile: e.mobile,
    role: 2,
    platformManagerDetail: managerRequest,
  });

  try {
    var config = {
      method: "POST",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/create-platform-manager`,

      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);

    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-manager"),
      // });
      if (res.data.success) {
        toast.success(res.data.message);

        Router.push("/platform-manager");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: CREATE_PLATFORM_MANAGER,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: CREATE_PLATFORM_MANAGER_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const forgetPassword = (e) => async (dispatch) => {
  try {
    let data = {
      email: e.email,
    };

    var config = {
      method: "POST",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/password/forget-password`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        // Authorization: `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNjc2NDc5OTc1LCJleHAiOjE2NzcwODQ3NzV9.uxzVeuxLY7KyRrE24f4hE0g1aD2kQjGreVHg4AQ8ARsWw97dnoCyeq4MAKhksiQPfvnOHJvJsLvAJGnq8B_yoQ`
      },
      data: data,
    };

    const res = await axios(config);
    if (res.data) {
      // Router.push("/token");
      toast.success(
        "A mail has been send to your email id for changing password"
      );
      dispatch({
        type: FORGET_PASSWORD,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: FORGET_PASSWORD_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const resetPassword = (e) => async (dispatch) => {
  try {
    let data = {
      resetToken: e.token,
      password: e.password,
    };
    // console.log("funciton call");
    var config = {
      method: "POST",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/password/update-new-password`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        // Authorization: `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNjc2NDc5OTc1LCJleHAiOjE2NzcwODQ3NzV9.uxzVeuxLY7KyRrE24f4hE0g1aD2kQjGreVHg4AQ8ARsWw97dnoCyeq4MAKhksiQPfvnOHJvJsLvAJGnq8B_yoQ`
      },
      data: data,
    };

    const res = await axios(config);
    if (res.data) {
      Router.push("/");
      toast.success("password changed successfully");
      dispatch({
        type: VERIFY_TOKEN,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: VERIFY_TOKEN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const verifyToken = (e) => async (dispatch) => {
  try {
    let token = e.token;
    // console.log("funciton call");
    var config = {
      method: "GET",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/password/verify-reset-token/${token}`,
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        // Authorization: `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNjc2NDc5OTc1LCJleHAiOjE2NzcwODQ3NzV9.uxzVeuxLY7KyRrE24f4hE0g1aD2kQjGreVHg4AQ8ARsWw97dnoCyeq4MAKhksiQPfvnOHJvJsLvAJGnq8B_yoQ`
      },
    };

    const res = await axios(config);
    if (res.data) {
      Router.push("/reset-password");
      dispatch({
        type: VERIFY_TOKEN,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: VERIFY_TOKEN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for update professional admin profile details
export const updateProfessionalAdmin = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  let adminRequest = {
    professionalAdminDetailId: e.professionalAdminDetailId,
    membershipNumber: "77777",
    contactAddress: e.contactAddress,
  };
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    mobile: e.mobile,
    professionalAdminDetail: adminRequest,
  });
  //let id = e.id;
  try {
    var config = {
      method: "PUT",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/update-professional-admin-profile/${e.id}`,

      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);

    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-manager"),
      // });
      if (res.data.success) {
        toast.success(res.data.message);

        Router.push("/professional-admin");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: UPDATE_PROFESSIONAL_ADMIN_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for update platform manager profile details
export const updatePlatformManager = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  let managerRequest = {
    platformManagerDetailId: e.platformManagerDetailId,
    contactAddress: e.contactAddress,
    uploadedDocument: "mmmmmmmmmmmm",
    validIdProof: "ttttttttttttttt",
  };
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    mobile: e.mobile,
    platformManagerDetail: managerRequest,
  });

  //let id = e.id;
  try {
    var config = {
      method: "PUT",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/update-platform-manager-profile/${e.id}`,

      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);

    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-manager"),
      // });
      if (res.data.success) {
        toast.success(res.data.message);

        Router.push("/platform-manager");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: UPDATE_PLATFORM_MANAGER_PROFILE_DETAILS_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for update platform user profile details
export const updatePlatformUser = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");

  let platformUserRequest = {
    platformUserDetailId: e.id,
    contactAddress: e.contactAddress,
    uploadedDocument: "yyyyyyyyyyyyyyyy",
    validIdProof: "kkkkkkkkkkkkkkkkkkkkkkkkk",
    platformManagerId: e.platformManagerId,
  };
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    mobile: e.mobile,
    platformUserRequest: platformUserRequest,
  });

  //let id = e.id;
  try {
    var config = {
      method: "PUT",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/update-platform-user-profile/${e.id}`,

      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);

    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-manager"),
      // });
      if (res.data.success) {
        toast.success(res.data.message);

        Router.push("/platform-user");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: UPDATE_PLATFORM_USER,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: UPDATE_PLATFORM_USER_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for update client admin profile details
export const updateClientAdmin = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  let clientAdminDetail = {
    clientAdminDetailId: e.clientAdminDetailId,
    companyName: e.companyName,
    panNumber: e.panNumber,
    gstNumber: e.gstNumber,
    businessType: e.businessType,
    communicationAddress: e.communicationAddress,
  };
  let data = JSON.stringify({
    firstName: e.firstName,
    lastName: e.lastName,
    mobile: e.mobile,
    clientAdminDetail: clientAdminDetail,
  });

  //let id = e.id;
  try {
    var config = {
      method: "PUT",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/update-client-admin-profile/${e.id}`,

      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${process.env.NEXT_PUBLIC_TOCKEN}`,
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    const res = await axios(config);
    if (res.data) {
      // toast.success("created successfully", {
      //   onClose: () => Router.push("/platform-manager"),
      // });
      if (res.data.success) {
        toast.success(res.data.message);

        Router.push("/client-admin");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: UPDATE_CLIENT_ADMIN,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: UPDATE_CLIENT_ADMIN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

//Action dispatcher for verify Professional Admin
export const verifyProfessionalAdmin = (e) => async (dispatch) => {
  try {
    let token = localStorage.getItem("token_key");
    var config = {
      method: "PATCH",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/verify-account/1/${e}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        // Authorization: `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNjc2NDc5OTc1LCJleHAiOjE2NzcwODQ3NzV9.uxzVeuxLY7KyRrE24f4hE0g1aD2kQjGreVHg4AQ8ARsWw97dnoCyeq4MAKhksiQPfvnOHJvJsLvAJGnq8B_yoQ`
      },
    };

    const res = await axios(config);
    if (res.data) {
      // Router.push("/professional-admin");
      if (res.data.success) {
        toast.success(res.data.message);
      } else {
        toast.danger(res.data.message);
      }

      dispatch({
        type: VERIFY_PROFESSIONAL_ADMIN,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: VERIFY_PROFESSIONAL_ADMIN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};

export const deletePlatformManager = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "DELETE",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/delete-platform-manager/${e.id}/${e.platformManagerDetailId}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const res = await axios(config);
    if (res.data) {
      if (res.data.success) {
        toast.success(res.data.message);
        Router.push("/platform-manager");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: DELTE_PLATFORM_MANAGER,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: DELTE_PLATFORM_MANAGER_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};
export const deletePlatformUser = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "DELETE",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/delete-platform-user/${e.id}/${e.platformUserDetailId}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const res = await axios(config);
    if (res.data) {
      if (res.data.success) {
        toast.success(res.data.message);
        Router.push("/platform-user");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: DELETE_PLATFORM_USER,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: DELETE_PLATFORM_USER_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};
export const deletePlatformProfessionalAdmin = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "DELETE",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/delete-professional-admin/${e.id}/${e.professionalAdminDetailId}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const res = await axios(config);

    if (res.data) {
      if (res.data.success) {
        toast.success(res.data.message);
        Router.push("/professional-admin");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: DELETE_PROFESSIONAL_ADMIN,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: DELETE_PROFESSIONAL_ADMIN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};
export const deletePlatformClientAdmin = (e) => async (dispatch) => {
  let token = localStorage.getItem("token_key");
  try {
    var config = {
      method: "DELETE",
      url: `${process.env.NEXT_PUBLIC_API_URL}api/platform-admin/delete-client-admin/${e.id}/${e.clientAdminDetailId}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const res = await axios(config);
    if (res.data) {
      if (res.data.success) {
        toast.success(res.data.message);
        Router.push("/client-admin");
      } else {
        toast.error(res.response.data.message);
      }

      dispatch({
        type: DELTE_CLIENT_ADMIN,
        payload: res.data,
      });
    }
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: DELTE_CLIENT_ADMIN_ERROR,
      payload: error?.response?.error?.message,
    });
  }
};
