import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import CreatePlatformManager from "@/components/platform-manager-admin/create-manager";
import ProfessionalManagerCard from "@/components/professional-manager-card";
//import CreatePlatformUser from "@/components/platform-user/create-user";
import SideMenu from "@/components/sideMenu";


export default function ProfessionalManagerCreate() {
  return <>
    <div className='admin_wrap container-fluid'>
      {/* SideMenu*/}
      <SideMenu />
      <div className="admin-content-area">
        {/* AdminHeader*/}
        <AdminHeader />
        <div className="dashboard-widget-wrap">
          {/* CreatePlatformManager*/}
          <CreatePlatformManager />
        </div>
      </div>
    </div>
    <Footer />
  </>
}