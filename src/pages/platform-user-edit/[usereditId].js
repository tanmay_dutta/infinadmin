import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { platformUserProfileDetails } from "store/actions/platformAdminAction";
import { useForm } from "@mantine/form";
import { Button, NativeSelect, TextInput } from "@mantine/core";
import { Dropzone } from "@mantine/dropzone";
import { toast } from "react-toastify";
import { updatePlatformUser } from "store/actions/platformAdminAction";

export default function ClientDetailsEdit() {

  const router = useRouter()
  const usereditId = router.query.usereditId;
  console.log("professional admin id: ", usereditId);
  const dispatch = useDispatch();
  useEffect(() => {
    if (usereditId) {
      dispatch(platformUserProfileDetails(usereditId)); // action calling
    }


  }, [usereditId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.platformUserProfileDetails);
  console.log("result of user: ", result);

  useEffect(() => {
    form.setValues((prev) => ({ ...prev, ...result }));
  }, [result])

  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      //email: "",
      mobile: "",
      contactAddress: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  const openRef = useRef(null);

  return (
    <>


      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/*AdminHeader*/}
          <AdminHeader />
          <div className="card admin-card-wedget">
            <form
              onSubmit={form.onSubmit((values) => {

                // toast.success("Client Admin Details Updated Successfully");
                console.log(values, "=======");
                dispatch(updatePlatformUser(values));
                // router.push("/platform-user")
              }
              )}
            >
              <div className="card-body p-0">
                {
                  result ?
                    <div className="row side-gap-0">
                      
                        <div className="col-lg-6 item">
                          <TextInput placeholder="Name" label="First name"  {...form.getInputProps("firstName")} />
                        </div>
                        <div className="col-lg-6 item">
                          <TextInput placeholder="Name" label="Last Name"  {...form.getInputProps("lastName")} />
                        </div>

                      
                      <div className="item">
                        <TextInput placeholder="Manager" label="Manager"   {...form.getInputProps("platformManagerId")} />
                      </div>
                      <div className="item">
                        <TextInput placeholder="Contact Number" label="Contact Number"  {...form.getInputProps("mobile")} />

                      </div>
                      <div className="item">
                        <TextInput placeholder="Email address" label="Email address"  {...form.getInputProps("email")} />

                      </div>
                      <div className="item">
                        <TextInput placeholder="Uploaded Document" label="Uploaded Document"  {...form.getInputProps("uploadedDocument")} />

                      </div>
                      <div className="item">
                        <TextInput placeholder="Contact address" label="Contact address"  {...form.getInputProps("contactAddress")} />

                      </div>
                      <div className="item">
                        <TextInput placeholder="Valid Proof" label="Valid Proof"  {...form.getInputProps("validIdProof")} />

                      </div>

                      {/* Update Button*/}

                      <div className="col-lg-12 actions place-center mt-5">
                        <Button type="submit">Update</Button>
                      </div>
                    </div>
                    : null
                }
              </div>
            </form>
          </div>
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}

