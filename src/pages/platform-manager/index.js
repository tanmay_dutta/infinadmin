import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import PlatformManagerCard from "@/components/list/PlatformManagerCard";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { platformManagerList } from "store/actions/platformAdminAction";
import LoadingScreen from "../loading";
import CsvDownload from "@/components/CsvDownload";
//Main
export default function PlatformAdmin() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(platformManagerList()); //calling the action
  }, []);
  const result = useSelector(
    (state) => state.platformAdminReducer.platformManagerList
  );
  const professional_Admin_List = result.content;
  console.log("total list is",professional_Admin_List);

  const headers = [
    { label: "ID", key: "id" },
    { label: "firstName", key: "firstName" },
    { label: "lastName", key: "lastName" },
    { label: "Contact Address", key: "contactAddress" },
    { label: "Email", key: "email" },
    { label: "Platform Manager DetailId", key: "platformManagerDetailId" },
    { label: "Contact Number", key: "mobile" },
    { label: "Uploaded Document", key: "uploadedDocument" },
    { label: "ValidIdProof", key: "validIdProof" },
  ];


  const [userdata, setUserdata] = useState();
  useEffect(() => {
    setUserdata(professional_Admin_List );
  }, [professional_Admin_List ]);

  return (
    <>
      <div className="admin_wrap container-fluid">
        {/* SideMenu*/}
        <SideMenu />
        <div className="admin-content-area">
          {/* AdminHeader*/}
          <AdminHeader />

          {/* Download list*/}
          <div className="top-filter-btns-group ms-auto d-flex flex-wrap">
           

            {professional_Admin_List  ? (
                <CsvDownload
                  data={professional_Admin_List}
                  headers={headers}
                  filename={"platform Manager Detail"}
                />
              ) : (
                ""
              )}
          </div>
          {professional_Admin_List !== undefined ? (
            <div className="dashboard-widget-wrap">
              {professional_Admin_List?.map((item, key) => {
                return (
                  //ProfessionalAdminCard
                  <PlatformManagerCard
                    id={item.id}
                    contactAddress={item.contactAddress}
                    firstName={item.firstName}
                    lastName={item.lastName}
                    email={item.email}
                    platformManagerDetailId={item.platformManagerDetailId}
                    mobile={item.mobile}
                    uploadedDocument={item.uploadedDocument}
                    validIdProof={item.validIdProof}
                    isVerified={item.isVerified}
                    key={`prop-detail-list-${key}`}
                  />
                );
              })}
            </div>
          ) : (
            <LoadingScreen />
          )}
        </div>
      </div>
      {/* Footer*/}
      <Footer />
    </>
  );
}
