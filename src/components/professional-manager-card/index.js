import { Avatar, Menu } from "@mantine/core";
import DownloadIcon from "@/components/icon/download";
import VerticalDot from "@/components/icon/vertical-dot";

//main component for ProfessionalManagerCard
export default function ProfessionalManagerCard({props}) {
    return <div className="card admin-card-wedget">
    <div className="card-head">
      {/* Avatar card-image*/}
        <div className="card-image">
        <Avatar src="/avatar_img.png" alt="Some alt text" size={78} />
         </div>
         {/* Serial Number*/}
        <div className="info">
          <p className="card-text mb-1">Serial Number: {props.membershipNumber}</p>
          <h3 className="card-title">{props.name}</h3>
        </div>
         {/* Download details*/}
        <div className="btns my-auto">
          <a href="" className="btn btn-sm btn-light-blue"><span className="icon"><DownloadIcon />
         </span>Download details</a>
        </div>
        {/* Menu Dropdown with VerticalDot*/}
        <Menu shadow="md" width={200} classNames="threedot-drops-menu">
        <Menu.Target>
          <div className="three-dot-btn"><VerticalDot /></div>
        </Menu.Target>
        <Menu.Dropdown>
          <Menu.Item icon={<VerticalDot />}>View</Menu.Item>
          <Menu.Item icon={<VerticalDot />}>Edit</Menu.Item>
          <Menu.Item icon={<VerticalDot />}>Delete</Menu.Item>

        </Menu.Dropdown>
        </Menu>
    </div>
    <div className="card-body">
      <div className="key-val-lists row">
        {/* Contact number*/}
        <div className="item col-lg-6">
          <span className="key">Contact number: </span>
          <span className="val">{props.mobile}</span>
        </div>
        {/* Years of experience*/}
        <div className="item col-lg-6">
          <span className="key">Years of experience:  </span>
          <span className="val">10 years +</span>
        </div>
        {/* Email ID*/}
        <div className="item col-lg-6">
          <span className="key">Email ID:  </span>
          <span className="val">{props.email}</span>
        </div>
        {/* Subscribed plan*/}
        <div className="item col-lg-6">
          <span className="key">Subscribed plan:  </span>
          <span className="val"><span className="pill pill-light-blue">Professional</span></span>
        </div>
        {/* Status*/}
        <div className="item col-lg-12">
          <span className="key">Status: </span>
          <span className="val"> FCA</span>
        </div>
        {/*Contact Address*/}
        <div className="item col-lg-12">
          <span className="key">Contact Address:  </span>
          <span className="val"> {props.contactAddress}</span>
        </div>
        
      </div>
    </div>
  </div>
  }