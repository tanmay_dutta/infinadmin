import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { platformUserProfileDetails } from "store/actions/platformAdminAction";
import { useRouter } from "next/router";

export default function PlatformUserDetails() {
  const dispatch = useDispatch();
  const router = useRouter();
  const userId = router.query.userId;
  useEffect(() => {
    if (userId) {
    dispatch(platformUserProfileDetails(userId));
    }
  }, [userId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.platformUserProfileDetails
  );
  console.log("result of platform user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">{result.firstName} {" "} {result.lastName}</span>
                </div>
                <div className="item">
                  <span className="key">Manager: </span>
                  <span className="val">{result.platformManagerId}</span>
                </div>
                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                <div className="item">
                  <span className="key">Uploaded Document: </span>
                  <span className="val">{result.uploadedDocument}</span>
                </div>
                <div className="item">
                  <span className="key">Contact address: </span>
                  <span className="val">{result.contactAddress}</span>
                </div>
                <div className="item">
                  <span className="key">Valid Proof: </span>
                  <span className="val">{result.validIdProof}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
