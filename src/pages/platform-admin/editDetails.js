import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import { useForm } from "@mantine/form";
import SideMenu from "@/components/sideMenu";
import { Button, TextInput } from "@mantine/core";
import { useDispatch, useSelector } from "react-redux";
import { updatePlatformAdminProfileDetails } from "store/actions/platformAdminAction";
import { platFormAdminProfileDetails } from "store/actions/platformAdminAction";
import { useRef, useEffect } from "react";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

export default function SimpleDetailsEdit() {
  const openRef = useRef(null);
  const dispatch = useDispatch();
  const router = useRouter();
  useEffect(() => {
    dispatch(platFormAdminProfileDetails());
  }, []);
  const result = useSelector(
    (state) => state.platformAdminReducer.platformAdminData
  );
  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      mobile: "",
    },
    // functions will be used to validate values at corresponding key
    validate: {
      mobile: (value) =>
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value)
          ? null
          : "Invalid Mobile Number",
    },
  });
  useEffect(() => {
    form.setValues((prev) => ({
      ...prev,
      ...result,
      // firstName: result.name.substring(0, result.name.indexOf(" ")),
      // lastName: result.name.substring(result.name.indexOf(' '))
    }));
  }, [result]);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="card admin-card-wedget">
              <form
                onSubmit={form.onSubmit((values) =>
                 {
                  
                  dispatch(updatePlatformAdminProfileDetails(values))
                 
                }
                )}
              >
                <div className="card-body p-0">
                  <div className="row side-gap-0">
                    <div className="col-lg-6">
                      <TextInput
                        placeholder="First name"
                        {...form.getInputProps("firstName")}
                      />
                    </div>
                    <div className="col-lg-6">
                      <TextInput
                        placeholder="Last name"
                        {...form.getInputProps("lastName")}
                      />
                    </div>

                    <div className="col-lg-12">
                      <TextInput
                        placeholder="Contact number"
                        {...form.getInputProps("mobile")}
                      />
                    </div>

                    <div className="col-lg-12 actions place-center mt-5">
                      <Button type="submit">Update</Button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
