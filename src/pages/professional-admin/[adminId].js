import AdminHeader from "@/components/common/admin-header";
import Footer from "@/components/common/footer";
import SideMenu from "@/components/sideMenu";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { professionalAdminProfileDetails } from "store/actions/platformAdminAction";
import { useRouter } from "next/router";

export default function AdminDetails() {
  const router = useRouter();
  const adminId = router.query.adminId;
  console.log("professional admin id: ", adminId);
  const dispatch = useDispatch();
  useEffect(() => {
    if (adminId) {
      dispatch(professionalAdminProfileDetails(adminId));//action calling
    }
  }, [adminId, dispatch]);

  const result = useSelector(
    (state) => state.platformAdminReducer.professionalAdminDetails
  );
  console.log("result of professional admin user: ", result);

  return (
    <>
      <div className="admin_wrap container-fluid">
        <SideMenu />
        <div className="admin-content-area">
          <AdminHeader />
          <div className="dashboard-widget-wrap">
            <div className="block-content-box profile-info-details-card">
              <div className="key-val-items">
                {/* Name*/}
                <div className="item">
                  <span className="key">Name: </span>
                  <span className="val">{result.firstName}{" "}{result.lastName }</span>
                </div>
                {/* Contact Number*/}
                <div className="item">
                  <span className="key">Contact Number: </span>
                  <span className="val">{result.mobile}</span>
                </div>
                {/* Email address*/}
                <div className="item">
                  <span className="key">Email address: </span>
                  <span className="val"> {result.email}</span>
                </div>
                {/* <div className="item">
                <span className="key">Uploaded Document: </span>
                <span className="val">{result.uploadedDocument}</span>
              </div> */}
              {/* Membership Number*/}
                <div className="item">
                  <span className="key">Membership Number: </span>
                  <span className="val"> {result.membershipNumber}</span>
                </div>
              {/* Contact address*/}
                <div className="item">
                  <span className="key">Contact address: </span>
                  <span className="val">{result.contactAddress}</span>
                </div>
                {/* <div className="item">
                <span className="key">Valid Proof: </span>
                <span className="val">{result.validIdProof}</span>
              </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/*Footer*/}
      <Footer />
    </>
  );
}
